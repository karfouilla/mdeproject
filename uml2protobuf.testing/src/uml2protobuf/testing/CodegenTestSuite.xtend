package uml2protobuf.testing

import java.nio.file.Files
import java.nio.file.Paths
import java.util.List
import java.util.stream.Collectors
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import uml2protobuf.codegenerator.ProtobufCodeGenerator
import protobuf.PotoBufFile

/**
 * Run the IDLCodeGenerator on each IDL model found in "models/output/protobuf/*.xmi".
 * 
 * Results are produced in the same folder but with the '.thrift' file extension.
 */
class CodegenTestSuite {

	private def void generateCodeFrom(String modelPath) {
		// Load model
		val ResourceSet rs = new ResourceSetImpl()
		val Resource r = rs.getResource(URI::createFileURI(modelPath), true)
		val root = r.contents.get(0)

		// Generate code
		val result = new ProtobufCodeGenerator().generateCode(root as PotoBufFile)
		r.unload

		// Save as file
		val codePathString = modelPath.replace("xmi", "protobuf")
		val codePath = Paths::get(codePathString)
		Files::writeString(codePath, result)
	}

	@ParameterizedTest
	@MethodSource("modelProvider")
	def void generateCode(String argument) {
		generateCodeFrom(argument);
	}

	def static List<String> modelProvider() {
		return Files::list(Paths::get("models/output/protobuf/")).map([p|p.toString]).filter([s|s.endsWith(".xmi")]).collect(Collectors.toList())
	}

}
