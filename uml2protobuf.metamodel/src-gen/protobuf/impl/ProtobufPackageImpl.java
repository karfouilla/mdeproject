/**
 */
package protobuf.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

import protobuf.PBoolLiteral;
import protobuf.PConstant;
import protobuf.PEnumField;
import protobuf.PFloatLiteral;
import protobuf.PIdent;
import protobuf.PImport;
import protobuf.PImportType;
import protobuf.PIntLiteral;
import protobuf.PLiteral;
import protobuf.PMapField;
import protobuf.PMessage;
import protobuf.PMessageField;
import protobuf.POneOf;
import protobuf.POptionValue;
import protobuf.PPackage;
import protobuf.PPrimitive;
import protobuf.PRange;
import protobuf.PRemoteProcedureCall;
import protobuf.PReserved;
import protobuf.PService;
import protobuf.PStringLiteral;
import protobuf.PType;
import protobuf.PTypeEnum;
import protobuf.PTypePrimitive;
import protobuf.PotoBufFile;
import protobuf.ProtobufFactory;
import protobuf.ProtobufPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ProtobufPackageImpl extends EPackageImpl implements ProtobufPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pTypePrimitiveEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pTypeEnumEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pEnumFieldEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pOptionValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pConstantEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pIdentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pLiteralEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pStringLiteralEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pIntLiteralEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pFloatLiteralEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pBoolLiteralEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pMessageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pMessageFieldEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pOneOfEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pMapFieldEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pReservedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pRangeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass potoBufFileEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pServiceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pRemoteProcedureCallEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pImportEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pPackageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum pPrimitiveEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum pImportTypeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see protobuf.ProtobufPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ProtobufPackageImpl() {
		super(eNS_URI, ProtobufFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link ProtobufPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ProtobufPackage init() {
		if (isInited)
			return (ProtobufPackage) EPackage.Registry.INSTANCE.getEPackage(ProtobufPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredProtobufPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		ProtobufPackageImpl theProtobufPackage = registeredProtobufPackage instanceof ProtobufPackageImpl
				? (ProtobufPackageImpl) registeredProtobufPackage
				: new ProtobufPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		XMLTypePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theProtobufPackage.createPackageContents();

		// Initialize created meta-data
		theProtobufPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theProtobufPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ProtobufPackage.eNS_URI, theProtobufPackage);
		return theProtobufPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPType() {
		return pTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPTypePrimitive() {
		return pTypePrimitiveEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPTypePrimitive_Type() {
		return (EAttribute) pTypePrimitiveEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPTypeEnum() {
		return pTypeEnumEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPTypeEnum_Fields() {
		return (EReference) pTypeEnumEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPTypeEnum_Options() {
		return (EReference) pTypeEnumEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPTypeEnum_Name() {
		return (EAttribute) pTypeEnumEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPEnumField() {
		return pEnumFieldEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPEnumField_Name() {
		return (EAttribute) pEnumFieldEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPEnumField_Value() {
		return (EAttribute) pEnumFieldEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPEnumField_Options() {
		return (EReference) pEnumFieldEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPOptionValue() {
		return pOptionValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPOptionValue_Value() {
		return (EReference) pOptionValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPOptionValue_Name() {
		return (EAttribute) pOptionValueEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPConstant() {
		return pConstantEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPIdent() {
		return pIdentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPIdent_Name() {
		return (EAttribute) pIdentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPLiteral() {
		return pLiteralEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPStringLiteral() {
		return pStringLiteralEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPStringLiteral_Value() {
		return (EAttribute) pStringLiteralEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPIntLiteral() {
		return pIntLiteralEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPIntLiteral_Value() {
		return (EAttribute) pIntLiteralEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPFloatLiteral() {
		return pFloatLiteralEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPFloatLiteral_Value() {
		return (EAttribute) pFloatLiteralEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPBoolLiteral() {
		return pBoolLiteralEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPBoolLiteral_Value() {
		return (EAttribute) pBoolLiteralEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPMessage() {
		return pMessageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPMessage_Enums() {
		return (EReference) pMessageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPMessage_Messages() {
		return (EReference) pMessageEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPMessage_Options() {
		return (EReference) pMessageEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPMessage_Fields() {
		return (EReference) pMessageEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPMessage_Oneof() {
		return (EReference) pMessageEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPMessage_Reserved() {
		return (EReference) pMessageEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPMessage_Name() {
		return (EAttribute) pMessageEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPMessageField() {
		return pMessageFieldEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPMessageField_Options() {
		return (EReference) pMessageFieldEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPMessageField_Name() {
		return (EAttribute) pMessageFieldEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPMessageField_Repeated() {
		return (EAttribute) pMessageFieldEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPMessageField_Number() {
		return (EAttribute) pMessageFieldEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPMessageField_Type() {
		return (EReference) pMessageFieldEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPOneOf() {
		return pOneOfEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPOneOf_Name() {
		return (EAttribute) pOneOfEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPOneOf_Options() {
		return (EReference) pOneOfEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPOneOf_Fields() {
		return (EReference) pOneOfEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPMapField() {
		return pMapFieldEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPMapField_Name() {
		return (EAttribute) pMapFieldEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPMapField_Options() {
		return (EReference) pMapFieldEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPMapField_KeyType() {
		return (EReference) pMapFieldEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPMapField_ValueType() {
		return (EReference) pMapFieldEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPMapField_Number() {
		return (EAttribute) pMapFieldEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPReserved() {
		return pReservedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPReserved_ReservedNames() {
		return (EAttribute) pReservedEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPReserved_ReservedRanges() {
		return (EReference) pReservedEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPRange() {
		return pRangeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPRange_Lower() {
		return (EAttribute) pRangeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPRange_Upper() {
		return (EAttribute) pRangeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPotoBufFile() {
		return potoBufFileEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPotoBufFile_Enums() {
		return (EReference) potoBufFileEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPotoBufFile_Messages() {
		return (EReference) potoBufFileEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPotoBufFile_Services() {
		return (EReference) potoBufFileEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPotoBufFile_Options() {
		return (EReference) potoBufFileEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPotoBufFile_Imports() {
		return (EReference) potoBufFileEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPotoBufFile_Packages() {
		return (EReference) potoBufFileEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPService() {
		return pServiceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPService_Name() {
		return (EAttribute) pServiceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPService_Options() {
		return (EReference) pServiceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPService_Rpc() {
		return (EReference) pServiceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPRemoteProcedureCall() {
		return pRemoteProcedureCallEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPRemoteProcedureCall_Name() {
		return (EAttribute) pRemoteProcedureCallEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPRemoteProcedureCall_InputMessageType() {
		return (EReference) pRemoteProcedureCallEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPRemoteProcedureCall_ReturnMessageType() {
		return (EReference) pRemoteProcedureCallEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPRemoteProcedureCall_InputStream() {
		return (EAttribute) pRemoteProcedureCallEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPRemoteProcedureCall_ReturnStream() {
		return (EAttribute) pRemoteProcedureCallEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPRemoteProcedureCall_Options() {
		return (EReference) pRemoteProcedureCallEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPImport() {
		return pImportEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPImport_Name() {
		return (EAttribute) pImportEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPImport_Type() {
		return (EAttribute) pImportEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPPackage() {
		return pPackageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPPackage_Name() {
		return (EAttribute) pPackageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getPPrimitive() {
		return pPrimitiveEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getPImportType() {
		return pImportTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProtobufFactory getProtobufFactory() {
		return (ProtobufFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		pTypeEClass = createEClass(PTYPE);

		pTypePrimitiveEClass = createEClass(PTYPE_PRIMITIVE);
		createEAttribute(pTypePrimitiveEClass, PTYPE_PRIMITIVE__TYPE);

		pTypeEnumEClass = createEClass(PTYPE_ENUM);
		createEReference(pTypeEnumEClass, PTYPE_ENUM__FIELDS);
		createEReference(pTypeEnumEClass, PTYPE_ENUM__OPTIONS);
		createEAttribute(pTypeEnumEClass, PTYPE_ENUM__NAME);

		pEnumFieldEClass = createEClass(PENUM_FIELD);
		createEAttribute(pEnumFieldEClass, PENUM_FIELD__NAME);
		createEAttribute(pEnumFieldEClass, PENUM_FIELD__VALUE);
		createEReference(pEnumFieldEClass, PENUM_FIELD__OPTIONS);

		pOptionValueEClass = createEClass(POPTION_VALUE);
		createEReference(pOptionValueEClass, POPTION_VALUE__VALUE);
		createEAttribute(pOptionValueEClass, POPTION_VALUE__NAME);

		pConstantEClass = createEClass(PCONSTANT);

		pIdentEClass = createEClass(PIDENT);
		createEAttribute(pIdentEClass, PIDENT__NAME);

		pLiteralEClass = createEClass(PLITERAL);

		pStringLiteralEClass = createEClass(PSTRING_LITERAL);
		createEAttribute(pStringLiteralEClass, PSTRING_LITERAL__VALUE);

		pIntLiteralEClass = createEClass(PINT_LITERAL);
		createEAttribute(pIntLiteralEClass, PINT_LITERAL__VALUE);

		pFloatLiteralEClass = createEClass(PFLOAT_LITERAL);
		createEAttribute(pFloatLiteralEClass, PFLOAT_LITERAL__VALUE);

		pBoolLiteralEClass = createEClass(PBOOL_LITERAL);
		createEAttribute(pBoolLiteralEClass, PBOOL_LITERAL__VALUE);

		pMessageEClass = createEClass(PMESSAGE);
		createEReference(pMessageEClass, PMESSAGE__ENUMS);
		createEReference(pMessageEClass, PMESSAGE__MESSAGES);
		createEReference(pMessageEClass, PMESSAGE__OPTIONS);
		createEReference(pMessageEClass, PMESSAGE__FIELDS);
		createEReference(pMessageEClass, PMESSAGE__ONEOF);
		createEReference(pMessageEClass, PMESSAGE__RESERVED);
		createEAttribute(pMessageEClass, PMESSAGE__NAME);

		pMessageFieldEClass = createEClass(PMESSAGE_FIELD);
		createEReference(pMessageFieldEClass, PMESSAGE_FIELD__OPTIONS);
		createEAttribute(pMessageFieldEClass, PMESSAGE_FIELD__NAME);
		createEAttribute(pMessageFieldEClass, PMESSAGE_FIELD__REPEATED);
		createEAttribute(pMessageFieldEClass, PMESSAGE_FIELD__NUMBER);
		createEReference(pMessageFieldEClass, PMESSAGE_FIELD__TYPE);

		pOneOfEClass = createEClass(PONE_OF);
		createEAttribute(pOneOfEClass, PONE_OF__NAME);
		createEReference(pOneOfEClass, PONE_OF__OPTIONS);
		createEReference(pOneOfEClass, PONE_OF__FIELDS);

		pMapFieldEClass = createEClass(PMAP_FIELD);
		createEAttribute(pMapFieldEClass, PMAP_FIELD__NAME);
		createEReference(pMapFieldEClass, PMAP_FIELD__OPTIONS);
		createEReference(pMapFieldEClass, PMAP_FIELD__KEY_TYPE);
		createEReference(pMapFieldEClass, PMAP_FIELD__VALUE_TYPE);
		createEAttribute(pMapFieldEClass, PMAP_FIELD__NUMBER);

		pReservedEClass = createEClass(PRESERVED);
		createEAttribute(pReservedEClass, PRESERVED__RESERVED_NAMES);
		createEReference(pReservedEClass, PRESERVED__RESERVED_RANGES);

		pRangeEClass = createEClass(PRANGE);
		createEAttribute(pRangeEClass, PRANGE__LOWER);
		createEAttribute(pRangeEClass, PRANGE__UPPER);

		potoBufFileEClass = createEClass(POTO_BUF_FILE);
		createEReference(potoBufFileEClass, POTO_BUF_FILE__ENUMS);
		createEReference(potoBufFileEClass, POTO_BUF_FILE__MESSAGES);
		createEReference(potoBufFileEClass, POTO_BUF_FILE__SERVICES);
		createEReference(potoBufFileEClass, POTO_BUF_FILE__OPTIONS);
		createEReference(potoBufFileEClass, POTO_BUF_FILE__IMPORTS);
		createEReference(potoBufFileEClass, POTO_BUF_FILE__PACKAGES);

		pServiceEClass = createEClass(PSERVICE);
		createEAttribute(pServiceEClass, PSERVICE__NAME);
		createEReference(pServiceEClass, PSERVICE__OPTIONS);
		createEReference(pServiceEClass, PSERVICE__RPC);

		pRemoteProcedureCallEClass = createEClass(PREMOTE_PROCEDURE_CALL);
		createEAttribute(pRemoteProcedureCallEClass, PREMOTE_PROCEDURE_CALL__NAME);
		createEReference(pRemoteProcedureCallEClass, PREMOTE_PROCEDURE_CALL__INPUT_MESSAGE_TYPE);
		createEReference(pRemoteProcedureCallEClass, PREMOTE_PROCEDURE_CALL__RETURN_MESSAGE_TYPE);
		createEAttribute(pRemoteProcedureCallEClass, PREMOTE_PROCEDURE_CALL__INPUT_STREAM);
		createEAttribute(pRemoteProcedureCallEClass, PREMOTE_PROCEDURE_CALL__RETURN_STREAM);
		createEReference(pRemoteProcedureCallEClass, PREMOTE_PROCEDURE_CALL__OPTIONS);

		pImportEClass = createEClass(PIMPORT);
		createEAttribute(pImportEClass, PIMPORT__NAME);
		createEAttribute(pImportEClass, PIMPORT__TYPE);

		pPackageEClass = createEClass(PPACKAGE);
		createEAttribute(pPackageEClass, PPACKAGE__NAME);

		// Create enums
		pPrimitiveEEnum = createEEnum(PPRIMITIVE);
		pImportTypeEEnum = createEEnum(PIMPORT_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		XMLTypePackage theXMLTypePackage = (XMLTypePackage) EPackage.Registry.INSTANCE
				.getEPackage(XMLTypePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		pTypePrimitiveEClass.getESuperTypes().add(this.getPType());
		pTypeEnumEClass.getESuperTypes().add(this.getPType());
		pIdentEClass.getESuperTypes().add(this.getPConstant());
		pLiteralEClass.getESuperTypes().add(this.getPConstant());
		pStringLiteralEClass.getESuperTypes().add(this.getPLiteral());
		pIntLiteralEClass.getESuperTypes().add(this.getPLiteral());
		pFloatLiteralEClass.getESuperTypes().add(this.getPLiteral());
		pBoolLiteralEClass.getESuperTypes().add(this.getPLiteral());
		pMessageEClass.getESuperTypes().add(this.getPType());

		// Initialize classes, features, and operations; add parameters
		initEClass(pTypeEClass, PType.class, "PType", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(pTypePrimitiveEClass, PTypePrimitive.class, "PTypePrimitive", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPTypePrimitive_Type(), this.getPPrimitive(), "type", null, 1, 1, PTypePrimitive.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pTypeEnumEClass, PTypeEnum.class, "PTypeEnum", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPTypeEnum_Fields(), this.getPEnumField(), null, "fields", null, 0, -1, PTypeEnum.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPTypeEnum_Options(), this.getPOptionValue(), null, "options", null, 0, -1, PTypeEnum.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPTypeEnum_Name(), theXMLTypePackage.getString(), "name", null, 1, 1, PTypeEnum.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pEnumFieldEClass, PEnumField.class, "PEnumField", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPEnumField_Name(), theXMLTypePackage.getString(), "name", null, 1, 1, PEnumField.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPEnumField_Value(), theXMLTypePackage.getInt(), "value", null, 1, 1, PEnumField.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPEnumField_Options(), this.getPOptionValue(), null, "options", null, 0, -1, PEnumField.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pOptionValueEClass, POptionValue.class, "POptionValue", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPOptionValue_Value(), this.getPConstant(), null, "value", null, 1, 1, POptionValue.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPOptionValue_Name(), theXMLTypePackage.getString(), "name", null, 1, 1, POptionValue.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pConstantEClass, PConstant.class, "PConstant", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(pIdentEClass, PIdent.class, "PIdent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPIdent_Name(), theXMLTypePackage.getString(), "name", null, 1, 1, PIdent.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pLiteralEClass, PLiteral.class, "PLiteral", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(pStringLiteralEClass, PStringLiteral.class, "PStringLiteral", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPStringLiteral_Value(), theXMLTypePackage.getString(), "value", null, 1, 1,
				PStringLiteral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(pIntLiteralEClass, PIntLiteral.class, "PIntLiteral", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPIntLiteral_Value(), theXMLTypePackage.getInt(), "value", null, 1, 1, PIntLiteral.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pFloatLiteralEClass, PFloatLiteral.class, "PFloatLiteral", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPFloatLiteral_Value(), theXMLTypePackage.getFloat(), "value", null, 1, 1, PFloatLiteral.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pBoolLiteralEClass, PBoolLiteral.class, "PBoolLiteral", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPBoolLiteral_Value(), theXMLTypePackage.getBoolean(), "value", null, 1, 1, PBoolLiteral.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pMessageEClass, PMessage.class, "PMessage", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPMessage_Enums(), this.getPTypeEnum(), null, "enums", null, 0, -1, PMessage.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPMessage_Messages(), this.getPMessage(), null, "messages", null, 0, -1, PMessage.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPMessage_Options(), this.getPOptionValue(), null, "options", null, 0, -1, PMessage.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPMessage_Fields(), this.getPMessageField(), null, "fields", null, 0, -1, PMessage.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPMessage_Oneof(), this.getPOneOf(), null, "oneof", null, 0, -1, PMessage.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getPMessage_Reserved(), this.getPReserved(), null, "reserved", null, 0, -1, PMessage.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPMessage_Name(), theXMLTypePackage.getString(), "name", null, 1, 1, PMessage.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pMessageFieldEClass, PMessageField.class, "PMessageField", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPMessageField_Options(), this.getPOptionValue(), null, "options", null, 0, -1,
				PMessageField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPMessageField_Name(), theXMLTypePackage.getString(), "name", null, 1, 1, PMessageField.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPMessageField_Repeated(), theXMLTypePackage.getBoolean(), "repeated", null, 1, 1,
				PMessageField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getPMessageField_Number(), theXMLTypePackage.getInt(), "number", null, 1, 1, PMessageField.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPMessageField_Type(), this.getPType(), null, "type", null, 1, 1, PMessageField.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pOneOfEClass, POneOf.class, "POneOf", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPOneOf_Name(), theXMLTypePackage.getString(), "name", null, 1, 1, POneOf.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPOneOf_Options(), this.getPOptionValue(), null, "options", null, 0, -1, POneOf.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPOneOf_Fields(), this.getPMessageField(), null, "fields", null, 0, -1, POneOf.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pMapFieldEClass, PMapField.class, "PMapField", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPMapField_Name(), theXMLTypePackage.getString(), "name", null, 1, 1, PMapField.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPMapField_Options(), this.getPOptionValue(), null, "options", null, 0, -1, PMapField.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPMapField_KeyType(), this.getPTypePrimitive(), null, "keyType", null, 1, 1, PMapField.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPMapField_ValueType(), this.getPType(), null, "valueType", null, 1, 1, PMapField.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPMapField_Number(), theXMLTypePackage.getInt(), "number", null, 1, 1, PMapField.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pReservedEClass, PReserved.class, "PReserved", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPReserved_ReservedNames(), theXMLTypePackage.getString(), "reservedNames", null, 0, -1,
				PReserved.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getPReserved_ReservedRanges(), this.getPRange(), null, "reservedRanges", null, 0, -1,
				PReserved.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pRangeEClass, PRange.class, "PRange", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPRange_Lower(), theXMLTypePackage.getInt(), "lower", null, 1, 1, PRange.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPRange_Upper(), theXMLTypePackage.getInt(), "upper", null, 0, 1, PRange.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(potoBufFileEClass, PotoBufFile.class, "PotoBufFile", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPotoBufFile_Enums(), this.getPTypeEnum(), null, "enums", null, 0, -1, PotoBufFile.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPotoBufFile_Messages(), this.getPMessage(), null, "messages", null, 0, -1, PotoBufFile.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPotoBufFile_Services(), this.getPService(), null, "services", null, 0, -1, PotoBufFile.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPotoBufFile_Options(), this.getPOptionValue(), null, "options", null, 0, -1,
				PotoBufFile.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPotoBufFile_Imports(), this.getPImport(), null, "imports", null, 0, -1, PotoBufFile.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPotoBufFile_Packages(), this.getPPackage(), null, "packages", null, 0, -1, PotoBufFile.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pServiceEClass, PService.class, "PService", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPService_Name(), theXMLTypePackage.getString(), "name", null, 1, 1, PService.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPService_Options(), this.getPOptionValue(), null, "options", null, 0, -1, PService.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPService_Rpc(), this.getPRemoteProcedureCall(), null, "rpc", null, 0, -1, PService.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pRemoteProcedureCallEClass, PRemoteProcedureCall.class, "PRemoteProcedureCall", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPRemoteProcedureCall_Name(), theXMLTypePackage.getString(), "name", null, 1, 1,
				PRemoteProcedureCall.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPRemoteProcedureCall_InputMessageType(), this.getPMessage(), null, "inputMessageType", null,
				1, 1, PRemoteProcedureCall.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPRemoteProcedureCall_ReturnMessageType(), this.getPMessage(), null, "returnMessageType", null,
				1, 1, PRemoteProcedureCall.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPRemoteProcedureCall_InputStream(), theXMLTypePackage.getBoolean(), "inputStream", null, 1, 1,
				PRemoteProcedureCall.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPRemoteProcedureCall_ReturnStream(), theXMLTypePackage.getBoolean(), "returnStream", null, 1,
				1, PRemoteProcedureCall.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPRemoteProcedureCall_Options(), this.getPOptionValue(), null, "options", null, 0, -1,
				PRemoteProcedureCall.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pImportEClass, PImport.class, "PImport", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPImport_Name(), theXMLTypePackage.getString(), "name", null, 1, 1, PImport.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPImport_Type(), this.getPImportType(), "type", null, 0, 1, PImport.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pPackageEClass, PPackage.class, "PPackage", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPPackage_Name(), theXMLTypePackage.getString(), "name", null, 1, 1, PPackage.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(pPrimitiveEEnum, PPrimitive.class, "PPrimitive");
		addEEnumLiteral(pPrimitiveEEnum, PPrimitive.DOUBLE);
		addEEnumLiteral(pPrimitiveEEnum, PPrimitive.FLOAT);
		addEEnumLiteral(pPrimitiveEEnum, PPrimitive.INT32);
		addEEnumLiteral(pPrimitiveEEnum, PPrimitive.INT64);
		addEEnumLiteral(pPrimitiveEEnum, PPrimitive.UINT32);
		addEEnumLiteral(pPrimitiveEEnum, PPrimitive.UINT64);
		addEEnumLiteral(pPrimitiveEEnum, PPrimitive.SINT32);
		addEEnumLiteral(pPrimitiveEEnum, PPrimitive.SINT64);
		addEEnumLiteral(pPrimitiveEEnum, PPrimitive.FIXED32);
		addEEnumLiteral(pPrimitiveEEnum, PPrimitive.FIXED64);
		addEEnumLiteral(pPrimitiveEEnum, PPrimitive.SFIXED32);
		addEEnumLiteral(pPrimitiveEEnum, PPrimitive.SFIXED64);
		addEEnumLiteral(pPrimitiveEEnum, PPrimitive.BOOL);
		addEEnumLiteral(pPrimitiveEEnum, PPrimitive.STRING);
		addEEnumLiteral(pPrimitiveEEnum, PPrimitive.BYTES);

		initEEnum(pImportTypeEEnum, PImportType.class, "PImportType");
		addEEnumLiteral(pImportTypeEEnum, PImportType.WEAK);
		addEEnumLiteral(pImportTypeEEnum, PImportType.PUBLIC);

		// Create resource
		createResource(eNS_URI);
	}

} //ProtobufPackageImpl
