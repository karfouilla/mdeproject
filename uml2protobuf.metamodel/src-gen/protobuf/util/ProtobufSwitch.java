/**
 */
package protobuf.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import protobuf.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see protobuf.ProtobufPackage
 * @generated
 */
public class ProtobufSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ProtobufPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProtobufSwitch() {
		if (modelPackage == null) {
			modelPackage = ProtobufPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case ProtobufPackage.PTYPE: {
			PType pType = (PType) theEObject;
			T result = casePType(pType);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ProtobufPackage.PTYPE_PRIMITIVE: {
			PTypePrimitive pTypePrimitive = (PTypePrimitive) theEObject;
			T result = casePTypePrimitive(pTypePrimitive);
			if (result == null)
				result = casePType(pTypePrimitive);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ProtobufPackage.PTYPE_ENUM: {
			PTypeEnum pTypeEnum = (PTypeEnum) theEObject;
			T result = casePTypeEnum(pTypeEnum);
			if (result == null)
				result = casePType(pTypeEnum);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ProtobufPackage.PENUM_FIELD: {
			PEnumField pEnumField = (PEnumField) theEObject;
			T result = casePEnumField(pEnumField);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ProtobufPackage.POPTION_VALUE: {
			POptionValue pOptionValue = (POptionValue) theEObject;
			T result = casePOptionValue(pOptionValue);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ProtobufPackage.PCONSTANT: {
			PConstant pConstant = (PConstant) theEObject;
			T result = casePConstant(pConstant);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ProtobufPackage.PIDENT: {
			PIdent pIdent = (PIdent) theEObject;
			T result = casePIdent(pIdent);
			if (result == null)
				result = casePConstant(pIdent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ProtobufPackage.PLITERAL: {
			PLiteral pLiteral = (PLiteral) theEObject;
			T result = casePLiteral(pLiteral);
			if (result == null)
				result = casePConstant(pLiteral);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ProtobufPackage.PSTRING_LITERAL: {
			PStringLiteral pStringLiteral = (PStringLiteral) theEObject;
			T result = casePStringLiteral(pStringLiteral);
			if (result == null)
				result = casePLiteral(pStringLiteral);
			if (result == null)
				result = casePConstant(pStringLiteral);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ProtobufPackage.PINT_LITERAL: {
			PIntLiteral pIntLiteral = (PIntLiteral) theEObject;
			T result = casePIntLiteral(pIntLiteral);
			if (result == null)
				result = casePLiteral(pIntLiteral);
			if (result == null)
				result = casePConstant(pIntLiteral);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ProtobufPackage.PFLOAT_LITERAL: {
			PFloatLiteral pFloatLiteral = (PFloatLiteral) theEObject;
			T result = casePFloatLiteral(pFloatLiteral);
			if (result == null)
				result = casePLiteral(pFloatLiteral);
			if (result == null)
				result = casePConstant(pFloatLiteral);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ProtobufPackage.PBOOL_LITERAL: {
			PBoolLiteral pBoolLiteral = (PBoolLiteral) theEObject;
			T result = casePBoolLiteral(pBoolLiteral);
			if (result == null)
				result = casePLiteral(pBoolLiteral);
			if (result == null)
				result = casePConstant(pBoolLiteral);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ProtobufPackage.PMESSAGE: {
			PMessage pMessage = (PMessage) theEObject;
			T result = casePMessage(pMessage);
			if (result == null)
				result = casePType(pMessage);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ProtobufPackage.PMESSAGE_FIELD: {
			PMessageField pMessageField = (PMessageField) theEObject;
			T result = casePMessageField(pMessageField);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ProtobufPackage.PONE_OF: {
			POneOf pOneOf = (POneOf) theEObject;
			T result = casePOneOf(pOneOf);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ProtobufPackage.PMAP_FIELD: {
			PMapField pMapField = (PMapField) theEObject;
			T result = casePMapField(pMapField);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ProtobufPackage.PRESERVED: {
			PReserved pReserved = (PReserved) theEObject;
			T result = casePReserved(pReserved);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ProtobufPackage.PRANGE: {
			PRange pRange = (PRange) theEObject;
			T result = casePRange(pRange);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ProtobufPackage.POTO_BUF_FILE: {
			PotoBufFile potoBufFile = (PotoBufFile) theEObject;
			T result = casePotoBufFile(potoBufFile);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ProtobufPackage.PSERVICE: {
			PService pService = (PService) theEObject;
			T result = casePService(pService);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ProtobufPackage.PREMOTE_PROCEDURE_CALL: {
			PRemoteProcedureCall pRemoteProcedureCall = (PRemoteProcedureCall) theEObject;
			T result = casePRemoteProcedureCall(pRemoteProcedureCall);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ProtobufPackage.PIMPORT: {
			PImport pImport = (PImport) theEObject;
			T result = casePImport(pImport);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ProtobufPackage.PPACKAGE: {
			PPackage pPackage = (PPackage) theEObject;
			T result = casePPackage(pPackage);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PType</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PType</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePType(PType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PType Primitive</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PType Primitive</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePTypePrimitive(PTypePrimitive object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PType Enum</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PType Enum</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePTypeEnum(PTypeEnum object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PEnum Field</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PEnum Field</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePEnumField(PEnumField object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>POption Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>POption Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePOptionValue(POptionValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PConstant</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PConstant</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePConstant(PConstant object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PIdent</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PIdent</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePIdent(PIdent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PLiteral</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PLiteral</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePLiteral(PLiteral object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PString Literal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PString Literal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePStringLiteral(PStringLiteral object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PInt Literal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PInt Literal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePIntLiteral(PIntLiteral object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PFloat Literal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PFloat Literal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePFloatLiteral(PFloatLiteral object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PBool Literal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PBool Literal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePBoolLiteral(PBoolLiteral object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PMessage</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PMessage</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePMessage(PMessage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PMessage Field</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PMessage Field</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePMessageField(PMessageField object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>POne Of</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>POne Of</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePOneOf(POneOf object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PMap Field</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PMap Field</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePMapField(PMapField object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PReserved</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PReserved</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePReserved(PReserved object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PRange</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PRange</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePRange(PRange object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Poto Buf File</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Poto Buf File</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePotoBufFile(PotoBufFile object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PService</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PService</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePService(PService object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PRemote Procedure Call</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PRemote Procedure Call</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePRemoteProcedureCall(PRemoteProcedureCall object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PImport</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PImport</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePImport(PImport object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PPackage</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PPackage</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePPackage(PPackage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //ProtobufSwitch
