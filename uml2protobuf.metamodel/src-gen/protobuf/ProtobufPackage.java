/**
 */
package protobuf;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see protobuf.ProtobufFactory
 * @model kind="package"
 * @generated
 */
public interface ProtobufPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "protobuf";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.example.org/metamodel";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "protobuf";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ProtobufPackage eINSTANCE = protobuf.impl.ProtobufPackageImpl.init();

	/**
	 * The meta object id for the '{@link protobuf.impl.PTypeImpl <em>PType</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see protobuf.impl.PTypeImpl
	 * @see protobuf.impl.ProtobufPackageImpl#getPType()
	 * @generated
	 */
	int PTYPE = 0;

	/**
	 * The number of structural features of the '<em>PType</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PTYPE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>PType</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PTYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link protobuf.impl.PTypePrimitiveImpl <em>PType Primitive</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see protobuf.impl.PTypePrimitiveImpl
	 * @see protobuf.impl.ProtobufPackageImpl#getPTypePrimitive()
	 * @generated
	 */
	int PTYPE_PRIMITIVE = 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PTYPE_PRIMITIVE__TYPE = PTYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>PType Primitive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PTYPE_PRIMITIVE_FEATURE_COUNT = PTYPE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>PType Primitive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PTYPE_PRIMITIVE_OPERATION_COUNT = PTYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link protobuf.impl.PTypeEnumImpl <em>PType Enum</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see protobuf.impl.PTypeEnumImpl
	 * @see protobuf.impl.ProtobufPackageImpl#getPTypeEnum()
	 * @generated
	 */
	int PTYPE_ENUM = 2;

	/**
	 * The feature id for the '<em><b>Fields</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PTYPE_ENUM__FIELDS = PTYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Options</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PTYPE_ENUM__OPTIONS = PTYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PTYPE_ENUM__NAME = PTYPE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>PType Enum</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PTYPE_ENUM_FEATURE_COUNT = PTYPE_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>PType Enum</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PTYPE_ENUM_OPERATION_COUNT = PTYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link protobuf.impl.PEnumFieldImpl <em>PEnum Field</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see protobuf.impl.PEnumFieldImpl
	 * @see protobuf.impl.ProtobufPackageImpl#getPEnumField()
	 * @generated
	 */
	int PENUM_FIELD = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PENUM_FIELD__NAME = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PENUM_FIELD__VALUE = 1;

	/**
	 * The feature id for the '<em><b>Options</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PENUM_FIELD__OPTIONS = 2;

	/**
	 * The number of structural features of the '<em>PEnum Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PENUM_FIELD_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>PEnum Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PENUM_FIELD_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link protobuf.impl.POptionValueImpl <em>POption Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see protobuf.impl.POptionValueImpl
	 * @see protobuf.impl.ProtobufPackageImpl#getPOptionValue()
	 * @generated
	 */
	int POPTION_VALUE = 4;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POPTION_VALUE__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POPTION_VALUE__NAME = 1;

	/**
	 * The number of structural features of the '<em>POption Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POPTION_VALUE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>POption Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POPTION_VALUE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link protobuf.impl.PConstantImpl <em>PConstant</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see protobuf.impl.PConstantImpl
	 * @see protobuf.impl.ProtobufPackageImpl#getPConstant()
	 * @generated
	 */
	int PCONSTANT = 5;

	/**
	 * The number of structural features of the '<em>PConstant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PCONSTANT_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>PConstant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PCONSTANT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link protobuf.impl.PIdentImpl <em>PIdent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see protobuf.impl.PIdentImpl
	 * @see protobuf.impl.ProtobufPackageImpl#getPIdent()
	 * @generated
	 */
	int PIDENT = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIDENT__NAME = PCONSTANT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>PIdent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIDENT_FEATURE_COUNT = PCONSTANT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>PIdent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIDENT_OPERATION_COUNT = PCONSTANT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link protobuf.impl.PLiteralImpl <em>PLiteral</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see protobuf.impl.PLiteralImpl
	 * @see protobuf.impl.ProtobufPackageImpl#getPLiteral()
	 * @generated
	 */
	int PLITERAL = 7;

	/**
	 * The number of structural features of the '<em>PLiteral</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLITERAL_FEATURE_COUNT = PCONSTANT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>PLiteral</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLITERAL_OPERATION_COUNT = PCONSTANT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link protobuf.impl.PStringLiteralImpl <em>PString Literal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see protobuf.impl.PStringLiteralImpl
	 * @see protobuf.impl.ProtobufPackageImpl#getPStringLiteral()
	 * @generated
	 */
	int PSTRING_LITERAL = 8;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSTRING_LITERAL__VALUE = PLITERAL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>PString Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSTRING_LITERAL_FEATURE_COUNT = PLITERAL_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>PString Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSTRING_LITERAL_OPERATION_COUNT = PLITERAL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link protobuf.impl.PIntLiteralImpl <em>PInt Literal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see protobuf.impl.PIntLiteralImpl
	 * @see protobuf.impl.ProtobufPackageImpl#getPIntLiteral()
	 * @generated
	 */
	int PINT_LITERAL = 9;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PINT_LITERAL__VALUE = PLITERAL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>PInt Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PINT_LITERAL_FEATURE_COUNT = PLITERAL_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>PInt Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PINT_LITERAL_OPERATION_COUNT = PLITERAL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link protobuf.impl.PFloatLiteralImpl <em>PFloat Literal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see protobuf.impl.PFloatLiteralImpl
	 * @see protobuf.impl.ProtobufPackageImpl#getPFloatLiteral()
	 * @generated
	 */
	int PFLOAT_LITERAL = 10;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PFLOAT_LITERAL__VALUE = PLITERAL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>PFloat Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PFLOAT_LITERAL_FEATURE_COUNT = PLITERAL_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>PFloat Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PFLOAT_LITERAL_OPERATION_COUNT = PLITERAL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link protobuf.impl.PBoolLiteralImpl <em>PBool Literal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see protobuf.impl.PBoolLiteralImpl
	 * @see protobuf.impl.ProtobufPackageImpl#getPBoolLiteral()
	 * @generated
	 */
	int PBOOL_LITERAL = 11;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PBOOL_LITERAL__VALUE = PLITERAL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>PBool Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PBOOL_LITERAL_FEATURE_COUNT = PLITERAL_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>PBool Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PBOOL_LITERAL_OPERATION_COUNT = PLITERAL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link protobuf.impl.PMessageImpl <em>PMessage</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see protobuf.impl.PMessageImpl
	 * @see protobuf.impl.ProtobufPackageImpl#getPMessage()
	 * @generated
	 */
	int PMESSAGE = 12;

	/**
	 * The feature id for the '<em><b>Enums</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PMESSAGE__ENUMS = PTYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Messages</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PMESSAGE__MESSAGES = PTYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Options</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PMESSAGE__OPTIONS = PTYPE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Fields</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PMESSAGE__FIELDS = PTYPE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Oneof</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PMESSAGE__ONEOF = PTYPE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Reserved</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PMESSAGE__RESERVED = PTYPE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PMESSAGE__NAME = PTYPE_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>PMessage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PMESSAGE_FEATURE_COUNT = PTYPE_FEATURE_COUNT + 7;

	/**
	 * The number of operations of the '<em>PMessage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PMESSAGE_OPERATION_COUNT = PTYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link protobuf.impl.PMessageFieldImpl <em>PMessage Field</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see protobuf.impl.PMessageFieldImpl
	 * @see protobuf.impl.ProtobufPackageImpl#getPMessageField()
	 * @generated
	 */
	int PMESSAGE_FIELD = 13;

	/**
	 * The feature id for the '<em><b>Options</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PMESSAGE_FIELD__OPTIONS = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PMESSAGE_FIELD__NAME = 1;

	/**
	 * The feature id for the '<em><b>Repeated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PMESSAGE_FIELD__REPEATED = 2;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PMESSAGE_FIELD__NUMBER = 3;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PMESSAGE_FIELD__TYPE = 4;

	/**
	 * The number of structural features of the '<em>PMessage Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PMESSAGE_FIELD_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>PMessage Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PMESSAGE_FIELD_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link protobuf.impl.POneOfImpl <em>POne Of</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see protobuf.impl.POneOfImpl
	 * @see protobuf.impl.ProtobufPackageImpl#getPOneOf()
	 * @generated
	 */
	int PONE_OF = 14;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PONE_OF__NAME = 0;

	/**
	 * The feature id for the '<em><b>Options</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PONE_OF__OPTIONS = 1;

	/**
	 * The feature id for the '<em><b>Fields</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PONE_OF__FIELDS = 2;

	/**
	 * The number of structural features of the '<em>POne Of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PONE_OF_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>POne Of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PONE_OF_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link protobuf.impl.PMapFieldImpl <em>PMap Field</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see protobuf.impl.PMapFieldImpl
	 * @see protobuf.impl.ProtobufPackageImpl#getPMapField()
	 * @generated
	 */
	int PMAP_FIELD = 15;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PMAP_FIELD__NAME = 0;

	/**
	 * The feature id for the '<em><b>Options</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PMAP_FIELD__OPTIONS = 1;

	/**
	 * The feature id for the '<em><b>Key Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PMAP_FIELD__KEY_TYPE = 2;

	/**
	 * The feature id for the '<em><b>Value Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PMAP_FIELD__VALUE_TYPE = 3;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PMAP_FIELD__NUMBER = 4;

	/**
	 * The number of structural features of the '<em>PMap Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PMAP_FIELD_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>PMap Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PMAP_FIELD_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link protobuf.impl.PReservedImpl <em>PReserved</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see protobuf.impl.PReservedImpl
	 * @see protobuf.impl.ProtobufPackageImpl#getPReserved()
	 * @generated
	 */
	int PRESERVED = 16;

	/**
	 * The feature id for the '<em><b>Reserved Names</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRESERVED__RESERVED_NAMES = 0;

	/**
	 * The feature id for the '<em><b>Reserved Ranges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRESERVED__RESERVED_RANGES = 1;

	/**
	 * The number of structural features of the '<em>PReserved</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRESERVED_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>PReserved</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRESERVED_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link protobuf.impl.PRangeImpl <em>PRange</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see protobuf.impl.PRangeImpl
	 * @see protobuf.impl.ProtobufPackageImpl#getPRange()
	 * @generated
	 */
	int PRANGE = 17;

	/**
	 * The feature id for the '<em><b>Lower</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRANGE__LOWER = 0;

	/**
	 * The feature id for the '<em><b>Upper</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRANGE__UPPER = 1;

	/**
	 * The number of structural features of the '<em>PRange</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRANGE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>PRange</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRANGE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link protobuf.impl.PotoBufFileImpl <em>Poto Buf File</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see protobuf.impl.PotoBufFileImpl
	 * @see protobuf.impl.ProtobufPackageImpl#getPotoBufFile()
	 * @generated
	 */
	int POTO_BUF_FILE = 18;

	/**
	 * The feature id for the '<em><b>Enums</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POTO_BUF_FILE__ENUMS = 0;

	/**
	 * The feature id for the '<em><b>Messages</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POTO_BUF_FILE__MESSAGES = 1;

	/**
	 * The feature id for the '<em><b>Services</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POTO_BUF_FILE__SERVICES = 2;

	/**
	 * The feature id for the '<em><b>Options</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POTO_BUF_FILE__OPTIONS = 3;

	/**
	 * The feature id for the '<em><b>Imports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POTO_BUF_FILE__IMPORTS = 4;

	/**
	 * The feature id for the '<em><b>Packages</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POTO_BUF_FILE__PACKAGES = 5;

	/**
	 * The number of structural features of the '<em>Poto Buf File</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POTO_BUF_FILE_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Poto Buf File</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POTO_BUF_FILE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link protobuf.impl.PServiceImpl <em>PService</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see protobuf.impl.PServiceImpl
	 * @see protobuf.impl.ProtobufPackageImpl#getPService()
	 * @generated
	 */
	int PSERVICE = 19;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSERVICE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Options</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSERVICE__OPTIONS = 1;

	/**
	 * The feature id for the '<em><b>Rpc</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSERVICE__RPC = 2;

	/**
	 * The number of structural features of the '<em>PService</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSERVICE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>PService</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSERVICE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link protobuf.impl.PRemoteProcedureCallImpl <em>PRemote Procedure Call</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see protobuf.impl.PRemoteProcedureCallImpl
	 * @see protobuf.impl.ProtobufPackageImpl#getPRemoteProcedureCall()
	 * @generated
	 */
	int PREMOTE_PROCEDURE_CALL = 20;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREMOTE_PROCEDURE_CALL__NAME = 0;

	/**
	 * The feature id for the '<em><b>Input Message Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREMOTE_PROCEDURE_CALL__INPUT_MESSAGE_TYPE = 1;

	/**
	 * The feature id for the '<em><b>Return Message Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREMOTE_PROCEDURE_CALL__RETURN_MESSAGE_TYPE = 2;

	/**
	 * The feature id for the '<em><b>Input Stream</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREMOTE_PROCEDURE_CALL__INPUT_STREAM = 3;

	/**
	 * The feature id for the '<em><b>Return Stream</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREMOTE_PROCEDURE_CALL__RETURN_STREAM = 4;

	/**
	 * The feature id for the '<em><b>Options</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREMOTE_PROCEDURE_CALL__OPTIONS = 5;

	/**
	 * The number of structural features of the '<em>PRemote Procedure Call</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREMOTE_PROCEDURE_CALL_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>PRemote Procedure Call</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREMOTE_PROCEDURE_CALL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link protobuf.impl.PImportImpl <em>PImport</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see protobuf.impl.PImportImpl
	 * @see protobuf.impl.ProtobufPackageImpl#getPImport()
	 * @generated
	 */
	int PIMPORT = 21;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIMPORT__NAME = 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIMPORT__TYPE = 1;

	/**
	 * The number of structural features of the '<em>PImport</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIMPORT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>PImport</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIMPORT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link protobuf.impl.PPackageImpl <em>PPackage</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see protobuf.impl.PPackageImpl
	 * @see protobuf.impl.ProtobufPackageImpl#getPPackage()
	 * @generated
	 */
	int PPACKAGE = 22;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PPACKAGE__NAME = 0;

	/**
	 * The number of structural features of the '<em>PPackage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PPACKAGE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>PPackage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PPACKAGE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link protobuf.PPrimitive <em>PPrimitive</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see protobuf.PPrimitive
	 * @see protobuf.impl.ProtobufPackageImpl#getPPrimitive()
	 * @generated
	 */
	int PPRIMITIVE = 23;

	/**
	 * The meta object id for the '{@link protobuf.PImportType <em>PImport Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see protobuf.PImportType
	 * @see protobuf.impl.ProtobufPackageImpl#getPImportType()
	 * @generated
	 */
	int PIMPORT_TYPE = 24;

	/**
	 * Returns the meta object for class '{@link protobuf.PType <em>PType</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>PType</em>'.
	 * @see protobuf.PType
	 * @generated
	 */
	EClass getPType();

	/**
	 * Returns the meta object for class '{@link protobuf.PTypePrimitive <em>PType Primitive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>PType Primitive</em>'.
	 * @see protobuf.PTypePrimitive
	 * @generated
	 */
	EClass getPTypePrimitive();

	/**
	 * Returns the meta object for the attribute '{@link protobuf.PTypePrimitive#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see protobuf.PTypePrimitive#getType()
	 * @see #getPTypePrimitive()
	 * @generated
	 */
	EAttribute getPTypePrimitive_Type();

	/**
	 * Returns the meta object for class '{@link protobuf.PTypeEnum <em>PType Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>PType Enum</em>'.
	 * @see protobuf.PTypeEnum
	 * @generated
	 */
	EClass getPTypeEnum();

	/**
	 * Returns the meta object for the containment reference list '{@link protobuf.PTypeEnum#getFields <em>Fields</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Fields</em>'.
	 * @see protobuf.PTypeEnum#getFields()
	 * @see #getPTypeEnum()
	 * @generated
	 */
	EReference getPTypeEnum_Fields();

	/**
	 * Returns the meta object for the containment reference list '{@link protobuf.PTypeEnum#getOptions <em>Options</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Options</em>'.
	 * @see protobuf.PTypeEnum#getOptions()
	 * @see #getPTypeEnum()
	 * @generated
	 */
	EReference getPTypeEnum_Options();

	/**
	 * Returns the meta object for the attribute '{@link protobuf.PTypeEnum#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see protobuf.PTypeEnum#getName()
	 * @see #getPTypeEnum()
	 * @generated
	 */
	EAttribute getPTypeEnum_Name();

	/**
	 * Returns the meta object for class '{@link protobuf.PEnumField <em>PEnum Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>PEnum Field</em>'.
	 * @see protobuf.PEnumField
	 * @generated
	 */
	EClass getPEnumField();

	/**
	 * Returns the meta object for the attribute '{@link protobuf.PEnumField#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see protobuf.PEnumField#getName()
	 * @see #getPEnumField()
	 * @generated
	 */
	EAttribute getPEnumField_Name();

	/**
	 * Returns the meta object for the attribute '{@link protobuf.PEnumField#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see protobuf.PEnumField#getValue()
	 * @see #getPEnumField()
	 * @generated
	 */
	EAttribute getPEnumField_Value();

	/**
	 * Returns the meta object for the containment reference list '{@link protobuf.PEnumField#getOptions <em>Options</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Options</em>'.
	 * @see protobuf.PEnumField#getOptions()
	 * @see #getPEnumField()
	 * @generated
	 */
	EReference getPEnumField_Options();

	/**
	 * Returns the meta object for class '{@link protobuf.POptionValue <em>POption Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>POption Value</em>'.
	 * @see protobuf.POptionValue
	 * @generated
	 */
	EClass getPOptionValue();

	/**
	 * Returns the meta object for the containment reference '{@link protobuf.POptionValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see protobuf.POptionValue#getValue()
	 * @see #getPOptionValue()
	 * @generated
	 */
	EReference getPOptionValue_Value();

	/**
	 * Returns the meta object for the attribute '{@link protobuf.POptionValue#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see protobuf.POptionValue#getName()
	 * @see #getPOptionValue()
	 * @generated
	 */
	EAttribute getPOptionValue_Name();

	/**
	 * Returns the meta object for class '{@link protobuf.PConstant <em>PConstant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>PConstant</em>'.
	 * @see protobuf.PConstant
	 * @generated
	 */
	EClass getPConstant();

	/**
	 * Returns the meta object for class '{@link protobuf.PIdent <em>PIdent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>PIdent</em>'.
	 * @see protobuf.PIdent
	 * @generated
	 */
	EClass getPIdent();

	/**
	 * Returns the meta object for the attribute '{@link protobuf.PIdent#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see protobuf.PIdent#getName()
	 * @see #getPIdent()
	 * @generated
	 */
	EAttribute getPIdent_Name();

	/**
	 * Returns the meta object for class '{@link protobuf.PLiteral <em>PLiteral</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>PLiteral</em>'.
	 * @see protobuf.PLiteral
	 * @generated
	 */
	EClass getPLiteral();

	/**
	 * Returns the meta object for class '{@link protobuf.PStringLiteral <em>PString Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>PString Literal</em>'.
	 * @see protobuf.PStringLiteral
	 * @generated
	 */
	EClass getPStringLiteral();

	/**
	 * Returns the meta object for the attribute '{@link protobuf.PStringLiteral#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see protobuf.PStringLiteral#getValue()
	 * @see #getPStringLiteral()
	 * @generated
	 */
	EAttribute getPStringLiteral_Value();

	/**
	 * Returns the meta object for class '{@link protobuf.PIntLiteral <em>PInt Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>PInt Literal</em>'.
	 * @see protobuf.PIntLiteral
	 * @generated
	 */
	EClass getPIntLiteral();

	/**
	 * Returns the meta object for the attribute '{@link protobuf.PIntLiteral#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see protobuf.PIntLiteral#getValue()
	 * @see #getPIntLiteral()
	 * @generated
	 */
	EAttribute getPIntLiteral_Value();

	/**
	 * Returns the meta object for class '{@link protobuf.PFloatLiteral <em>PFloat Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>PFloat Literal</em>'.
	 * @see protobuf.PFloatLiteral
	 * @generated
	 */
	EClass getPFloatLiteral();

	/**
	 * Returns the meta object for the attribute '{@link protobuf.PFloatLiteral#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see protobuf.PFloatLiteral#getValue()
	 * @see #getPFloatLiteral()
	 * @generated
	 */
	EAttribute getPFloatLiteral_Value();

	/**
	 * Returns the meta object for class '{@link protobuf.PBoolLiteral <em>PBool Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>PBool Literal</em>'.
	 * @see protobuf.PBoolLiteral
	 * @generated
	 */
	EClass getPBoolLiteral();

	/**
	 * Returns the meta object for the attribute '{@link protobuf.PBoolLiteral#isValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see protobuf.PBoolLiteral#isValue()
	 * @see #getPBoolLiteral()
	 * @generated
	 */
	EAttribute getPBoolLiteral_Value();

	/**
	 * Returns the meta object for class '{@link protobuf.PMessage <em>PMessage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>PMessage</em>'.
	 * @see protobuf.PMessage
	 * @generated
	 */
	EClass getPMessage();

	/**
	 * Returns the meta object for the containment reference list '{@link protobuf.PMessage#getEnums <em>Enums</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Enums</em>'.
	 * @see protobuf.PMessage#getEnums()
	 * @see #getPMessage()
	 * @generated
	 */
	EReference getPMessage_Enums();

	/**
	 * Returns the meta object for the containment reference list '{@link protobuf.PMessage#getMessages <em>Messages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Messages</em>'.
	 * @see protobuf.PMessage#getMessages()
	 * @see #getPMessage()
	 * @generated
	 */
	EReference getPMessage_Messages();

	/**
	 * Returns the meta object for the containment reference list '{@link protobuf.PMessage#getOptions <em>Options</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Options</em>'.
	 * @see protobuf.PMessage#getOptions()
	 * @see #getPMessage()
	 * @generated
	 */
	EReference getPMessage_Options();

	/**
	 * Returns the meta object for the containment reference list '{@link protobuf.PMessage#getFields <em>Fields</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Fields</em>'.
	 * @see protobuf.PMessage#getFields()
	 * @see #getPMessage()
	 * @generated
	 */
	EReference getPMessage_Fields();

	/**
	 * Returns the meta object for the containment reference list '{@link protobuf.PMessage#getOneof <em>Oneof</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Oneof</em>'.
	 * @see protobuf.PMessage#getOneof()
	 * @see #getPMessage()
	 * @generated
	 */
	EReference getPMessage_Oneof();

	/**
	 * Returns the meta object for the containment reference list '{@link protobuf.PMessage#getReserved <em>Reserved</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Reserved</em>'.
	 * @see protobuf.PMessage#getReserved()
	 * @see #getPMessage()
	 * @generated
	 */
	EReference getPMessage_Reserved();

	/**
	 * Returns the meta object for the attribute '{@link protobuf.PMessage#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see protobuf.PMessage#getName()
	 * @see #getPMessage()
	 * @generated
	 */
	EAttribute getPMessage_Name();

	/**
	 * Returns the meta object for class '{@link protobuf.PMessageField <em>PMessage Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>PMessage Field</em>'.
	 * @see protobuf.PMessageField
	 * @generated
	 */
	EClass getPMessageField();

	/**
	 * Returns the meta object for the containment reference list '{@link protobuf.PMessageField#getOptions <em>Options</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Options</em>'.
	 * @see protobuf.PMessageField#getOptions()
	 * @see #getPMessageField()
	 * @generated
	 */
	EReference getPMessageField_Options();

	/**
	 * Returns the meta object for the attribute '{@link protobuf.PMessageField#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see protobuf.PMessageField#getName()
	 * @see #getPMessageField()
	 * @generated
	 */
	EAttribute getPMessageField_Name();

	/**
	 * Returns the meta object for the attribute '{@link protobuf.PMessageField#isRepeated <em>Repeated</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Repeated</em>'.
	 * @see protobuf.PMessageField#isRepeated()
	 * @see #getPMessageField()
	 * @generated
	 */
	EAttribute getPMessageField_Repeated();

	/**
	 * Returns the meta object for the attribute '{@link protobuf.PMessageField#getNumber <em>Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number</em>'.
	 * @see protobuf.PMessageField#getNumber()
	 * @see #getPMessageField()
	 * @generated
	 */
	EAttribute getPMessageField_Number();

	/**
	 * Returns the meta object for the reference '{@link protobuf.PMessageField#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see protobuf.PMessageField#getType()
	 * @see #getPMessageField()
	 * @generated
	 */
	EReference getPMessageField_Type();

	/**
	 * Returns the meta object for class '{@link protobuf.POneOf <em>POne Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>POne Of</em>'.
	 * @see protobuf.POneOf
	 * @generated
	 */
	EClass getPOneOf();

	/**
	 * Returns the meta object for the attribute '{@link protobuf.POneOf#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see protobuf.POneOf#getName()
	 * @see #getPOneOf()
	 * @generated
	 */
	EAttribute getPOneOf_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link protobuf.POneOf#getOptions <em>Options</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Options</em>'.
	 * @see protobuf.POneOf#getOptions()
	 * @see #getPOneOf()
	 * @generated
	 */
	EReference getPOneOf_Options();

	/**
	 * Returns the meta object for the containment reference list '{@link protobuf.POneOf#getFields <em>Fields</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Fields</em>'.
	 * @see protobuf.POneOf#getFields()
	 * @see #getPOneOf()
	 * @generated
	 */
	EReference getPOneOf_Fields();

	/**
	 * Returns the meta object for class '{@link protobuf.PMapField <em>PMap Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>PMap Field</em>'.
	 * @see protobuf.PMapField
	 * @generated
	 */
	EClass getPMapField();

	/**
	 * Returns the meta object for the attribute '{@link protobuf.PMapField#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see protobuf.PMapField#getName()
	 * @see #getPMapField()
	 * @generated
	 */
	EAttribute getPMapField_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link protobuf.PMapField#getOptions <em>Options</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Options</em>'.
	 * @see protobuf.PMapField#getOptions()
	 * @see #getPMapField()
	 * @generated
	 */
	EReference getPMapField_Options();

	/**
	 * Returns the meta object for the reference '{@link protobuf.PMapField#getKeyType <em>Key Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key Type</em>'.
	 * @see protobuf.PMapField#getKeyType()
	 * @see #getPMapField()
	 * @generated
	 */
	EReference getPMapField_KeyType();

	/**
	 * Returns the meta object for the reference '{@link protobuf.PMapField#getValueType <em>Value Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value Type</em>'.
	 * @see protobuf.PMapField#getValueType()
	 * @see #getPMapField()
	 * @generated
	 */
	EReference getPMapField_ValueType();

	/**
	 * Returns the meta object for the attribute '{@link protobuf.PMapField#getNumber <em>Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number</em>'.
	 * @see protobuf.PMapField#getNumber()
	 * @see #getPMapField()
	 * @generated
	 */
	EAttribute getPMapField_Number();

	/**
	 * Returns the meta object for class '{@link protobuf.PReserved <em>PReserved</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>PReserved</em>'.
	 * @see protobuf.PReserved
	 * @generated
	 */
	EClass getPReserved();

	/**
	 * Returns the meta object for the attribute list '{@link protobuf.PReserved#getReservedNames <em>Reserved Names</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Reserved Names</em>'.
	 * @see protobuf.PReserved#getReservedNames()
	 * @see #getPReserved()
	 * @generated
	 */
	EAttribute getPReserved_ReservedNames();

	/**
	 * Returns the meta object for the containment reference list '{@link protobuf.PReserved#getReservedRanges <em>Reserved Ranges</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Reserved Ranges</em>'.
	 * @see protobuf.PReserved#getReservedRanges()
	 * @see #getPReserved()
	 * @generated
	 */
	EReference getPReserved_ReservedRanges();

	/**
	 * Returns the meta object for class '{@link protobuf.PRange <em>PRange</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>PRange</em>'.
	 * @see protobuf.PRange
	 * @generated
	 */
	EClass getPRange();

	/**
	 * Returns the meta object for the attribute '{@link protobuf.PRange#getLower <em>Lower</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lower</em>'.
	 * @see protobuf.PRange#getLower()
	 * @see #getPRange()
	 * @generated
	 */
	EAttribute getPRange_Lower();

	/**
	 * Returns the meta object for the attribute '{@link protobuf.PRange#getUpper <em>Upper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Upper</em>'.
	 * @see protobuf.PRange#getUpper()
	 * @see #getPRange()
	 * @generated
	 */
	EAttribute getPRange_Upper();

	/**
	 * Returns the meta object for class '{@link protobuf.PotoBufFile <em>Poto Buf File</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Poto Buf File</em>'.
	 * @see protobuf.PotoBufFile
	 * @generated
	 */
	EClass getPotoBufFile();

	/**
	 * Returns the meta object for the containment reference list '{@link protobuf.PotoBufFile#getEnums <em>Enums</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Enums</em>'.
	 * @see protobuf.PotoBufFile#getEnums()
	 * @see #getPotoBufFile()
	 * @generated
	 */
	EReference getPotoBufFile_Enums();

	/**
	 * Returns the meta object for the containment reference list '{@link protobuf.PotoBufFile#getMessages <em>Messages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Messages</em>'.
	 * @see protobuf.PotoBufFile#getMessages()
	 * @see #getPotoBufFile()
	 * @generated
	 */
	EReference getPotoBufFile_Messages();

	/**
	 * Returns the meta object for the containment reference list '{@link protobuf.PotoBufFile#getServices <em>Services</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Services</em>'.
	 * @see protobuf.PotoBufFile#getServices()
	 * @see #getPotoBufFile()
	 * @generated
	 */
	EReference getPotoBufFile_Services();

	/**
	 * Returns the meta object for the containment reference list '{@link protobuf.PotoBufFile#getOptions <em>Options</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Options</em>'.
	 * @see protobuf.PotoBufFile#getOptions()
	 * @see #getPotoBufFile()
	 * @generated
	 */
	EReference getPotoBufFile_Options();

	/**
	 * Returns the meta object for the containment reference list '{@link protobuf.PotoBufFile#getImports <em>Imports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Imports</em>'.
	 * @see protobuf.PotoBufFile#getImports()
	 * @see #getPotoBufFile()
	 * @generated
	 */
	EReference getPotoBufFile_Imports();

	/**
	 * Returns the meta object for the containment reference list '{@link protobuf.PotoBufFile#getPackages <em>Packages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Packages</em>'.
	 * @see protobuf.PotoBufFile#getPackages()
	 * @see #getPotoBufFile()
	 * @generated
	 */
	EReference getPotoBufFile_Packages();

	/**
	 * Returns the meta object for class '{@link protobuf.PService <em>PService</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>PService</em>'.
	 * @see protobuf.PService
	 * @generated
	 */
	EClass getPService();

	/**
	 * Returns the meta object for the attribute '{@link protobuf.PService#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see protobuf.PService#getName()
	 * @see #getPService()
	 * @generated
	 */
	EAttribute getPService_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link protobuf.PService#getOptions <em>Options</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Options</em>'.
	 * @see protobuf.PService#getOptions()
	 * @see #getPService()
	 * @generated
	 */
	EReference getPService_Options();

	/**
	 * Returns the meta object for the containment reference list '{@link protobuf.PService#getRpc <em>Rpc</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Rpc</em>'.
	 * @see protobuf.PService#getRpc()
	 * @see #getPService()
	 * @generated
	 */
	EReference getPService_Rpc();

	/**
	 * Returns the meta object for class '{@link protobuf.PRemoteProcedureCall <em>PRemote Procedure Call</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>PRemote Procedure Call</em>'.
	 * @see protobuf.PRemoteProcedureCall
	 * @generated
	 */
	EClass getPRemoteProcedureCall();

	/**
	 * Returns the meta object for the attribute '{@link protobuf.PRemoteProcedureCall#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see protobuf.PRemoteProcedureCall#getName()
	 * @see #getPRemoteProcedureCall()
	 * @generated
	 */
	EAttribute getPRemoteProcedureCall_Name();

	/**
	 * Returns the meta object for the reference '{@link protobuf.PRemoteProcedureCall#getInputMessageType <em>Input Message Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Input Message Type</em>'.
	 * @see protobuf.PRemoteProcedureCall#getInputMessageType()
	 * @see #getPRemoteProcedureCall()
	 * @generated
	 */
	EReference getPRemoteProcedureCall_InputMessageType();

	/**
	 * Returns the meta object for the reference '{@link protobuf.PRemoteProcedureCall#getReturnMessageType <em>Return Message Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Return Message Type</em>'.
	 * @see protobuf.PRemoteProcedureCall#getReturnMessageType()
	 * @see #getPRemoteProcedureCall()
	 * @generated
	 */
	EReference getPRemoteProcedureCall_ReturnMessageType();

	/**
	 * Returns the meta object for the attribute '{@link protobuf.PRemoteProcedureCall#isInputStream <em>Input Stream</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Input Stream</em>'.
	 * @see protobuf.PRemoteProcedureCall#isInputStream()
	 * @see #getPRemoteProcedureCall()
	 * @generated
	 */
	EAttribute getPRemoteProcedureCall_InputStream();

	/**
	 * Returns the meta object for the attribute '{@link protobuf.PRemoteProcedureCall#isReturnStream <em>Return Stream</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Return Stream</em>'.
	 * @see protobuf.PRemoteProcedureCall#isReturnStream()
	 * @see #getPRemoteProcedureCall()
	 * @generated
	 */
	EAttribute getPRemoteProcedureCall_ReturnStream();

	/**
	 * Returns the meta object for the containment reference list '{@link protobuf.PRemoteProcedureCall#getOptions <em>Options</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Options</em>'.
	 * @see protobuf.PRemoteProcedureCall#getOptions()
	 * @see #getPRemoteProcedureCall()
	 * @generated
	 */
	EReference getPRemoteProcedureCall_Options();

	/**
	 * Returns the meta object for class '{@link protobuf.PImport <em>PImport</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>PImport</em>'.
	 * @see protobuf.PImport
	 * @generated
	 */
	EClass getPImport();

	/**
	 * Returns the meta object for the attribute '{@link protobuf.PImport#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see protobuf.PImport#getName()
	 * @see #getPImport()
	 * @generated
	 */
	EAttribute getPImport_Name();

	/**
	 * Returns the meta object for the attribute '{@link protobuf.PImport#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see protobuf.PImport#getType()
	 * @see #getPImport()
	 * @generated
	 */
	EAttribute getPImport_Type();

	/**
	 * Returns the meta object for class '{@link protobuf.PPackage <em>PPackage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>PPackage</em>'.
	 * @see protobuf.PPackage
	 * @generated
	 */
	EClass getPPackage();

	/**
	 * Returns the meta object for the attribute '{@link protobuf.PPackage#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see protobuf.PPackage#getName()
	 * @see #getPPackage()
	 * @generated
	 */
	EAttribute getPPackage_Name();

	/**
	 * Returns the meta object for enum '{@link protobuf.PPrimitive <em>PPrimitive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>PPrimitive</em>'.
	 * @see protobuf.PPrimitive
	 * @generated
	 */
	EEnum getPPrimitive();

	/**
	 * Returns the meta object for enum '{@link protobuf.PImportType <em>PImport Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>PImport Type</em>'.
	 * @see protobuf.PImportType
	 * @generated
	 */
	EEnum getPImportType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ProtobufFactory getProtobufFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link protobuf.impl.PTypeImpl <em>PType</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see protobuf.impl.PTypeImpl
		 * @see protobuf.impl.ProtobufPackageImpl#getPType()
		 * @generated
		 */
		EClass PTYPE = eINSTANCE.getPType();

		/**
		 * The meta object literal for the '{@link protobuf.impl.PTypePrimitiveImpl <em>PType Primitive</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see protobuf.impl.PTypePrimitiveImpl
		 * @see protobuf.impl.ProtobufPackageImpl#getPTypePrimitive()
		 * @generated
		 */
		EClass PTYPE_PRIMITIVE = eINSTANCE.getPTypePrimitive();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PTYPE_PRIMITIVE__TYPE = eINSTANCE.getPTypePrimitive_Type();

		/**
		 * The meta object literal for the '{@link protobuf.impl.PTypeEnumImpl <em>PType Enum</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see protobuf.impl.PTypeEnumImpl
		 * @see protobuf.impl.ProtobufPackageImpl#getPTypeEnum()
		 * @generated
		 */
		EClass PTYPE_ENUM = eINSTANCE.getPTypeEnum();

		/**
		 * The meta object literal for the '<em><b>Fields</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PTYPE_ENUM__FIELDS = eINSTANCE.getPTypeEnum_Fields();

		/**
		 * The meta object literal for the '<em><b>Options</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PTYPE_ENUM__OPTIONS = eINSTANCE.getPTypeEnum_Options();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PTYPE_ENUM__NAME = eINSTANCE.getPTypeEnum_Name();

		/**
		 * The meta object literal for the '{@link protobuf.impl.PEnumFieldImpl <em>PEnum Field</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see protobuf.impl.PEnumFieldImpl
		 * @see protobuf.impl.ProtobufPackageImpl#getPEnumField()
		 * @generated
		 */
		EClass PENUM_FIELD = eINSTANCE.getPEnumField();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PENUM_FIELD__NAME = eINSTANCE.getPEnumField_Name();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PENUM_FIELD__VALUE = eINSTANCE.getPEnumField_Value();

		/**
		 * The meta object literal for the '<em><b>Options</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PENUM_FIELD__OPTIONS = eINSTANCE.getPEnumField_Options();

		/**
		 * The meta object literal for the '{@link protobuf.impl.POptionValueImpl <em>POption Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see protobuf.impl.POptionValueImpl
		 * @see protobuf.impl.ProtobufPackageImpl#getPOptionValue()
		 * @generated
		 */
		EClass POPTION_VALUE = eINSTANCE.getPOptionValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference POPTION_VALUE__VALUE = eINSTANCE.getPOptionValue_Value();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POPTION_VALUE__NAME = eINSTANCE.getPOptionValue_Name();

		/**
		 * The meta object literal for the '{@link protobuf.impl.PConstantImpl <em>PConstant</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see protobuf.impl.PConstantImpl
		 * @see protobuf.impl.ProtobufPackageImpl#getPConstant()
		 * @generated
		 */
		EClass PCONSTANT = eINSTANCE.getPConstant();

		/**
		 * The meta object literal for the '{@link protobuf.impl.PIdentImpl <em>PIdent</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see protobuf.impl.PIdentImpl
		 * @see protobuf.impl.ProtobufPackageImpl#getPIdent()
		 * @generated
		 */
		EClass PIDENT = eINSTANCE.getPIdent();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PIDENT__NAME = eINSTANCE.getPIdent_Name();

		/**
		 * The meta object literal for the '{@link protobuf.impl.PLiteralImpl <em>PLiteral</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see protobuf.impl.PLiteralImpl
		 * @see protobuf.impl.ProtobufPackageImpl#getPLiteral()
		 * @generated
		 */
		EClass PLITERAL = eINSTANCE.getPLiteral();

		/**
		 * The meta object literal for the '{@link protobuf.impl.PStringLiteralImpl <em>PString Literal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see protobuf.impl.PStringLiteralImpl
		 * @see protobuf.impl.ProtobufPackageImpl#getPStringLiteral()
		 * @generated
		 */
		EClass PSTRING_LITERAL = eINSTANCE.getPStringLiteral();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PSTRING_LITERAL__VALUE = eINSTANCE.getPStringLiteral_Value();

		/**
		 * The meta object literal for the '{@link protobuf.impl.PIntLiteralImpl <em>PInt Literal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see protobuf.impl.PIntLiteralImpl
		 * @see protobuf.impl.ProtobufPackageImpl#getPIntLiteral()
		 * @generated
		 */
		EClass PINT_LITERAL = eINSTANCE.getPIntLiteral();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PINT_LITERAL__VALUE = eINSTANCE.getPIntLiteral_Value();

		/**
		 * The meta object literal for the '{@link protobuf.impl.PFloatLiteralImpl <em>PFloat Literal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see protobuf.impl.PFloatLiteralImpl
		 * @see protobuf.impl.ProtobufPackageImpl#getPFloatLiteral()
		 * @generated
		 */
		EClass PFLOAT_LITERAL = eINSTANCE.getPFloatLiteral();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PFLOAT_LITERAL__VALUE = eINSTANCE.getPFloatLiteral_Value();

		/**
		 * The meta object literal for the '{@link protobuf.impl.PBoolLiteralImpl <em>PBool Literal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see protobuf.impl.PBoolLiteralImpl
		 * @see protobuf.impl.ProtobufPackageImpl#getPBoolLiteral()
		 * @generated
		 */
		EClass PBOOL_LITERAL = eINSTANCE.getPBoolLiteral();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PBOOL_LITERAL__VALUE = eINSTANCE.getPBoolLiteral_Value();

		/**
		 * The meta object literal for the '{@link protobuf.impl.PMessageImpl <em>PMessage</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see protobuf.impl.PMessageImpl
		 * @see protobuf.impl.ProtobufPackageImpl#getPMessage()
		 * @generated
		 */
		EClass PMESSAGE = eINSTANCE.getPMessage();

		/**
		 * The meta object literal for the '<em><b>Enums</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PMESSAGE__ENUMS = eINSTANCE.getPMessage_Enums();

		/**
		 * The meta object literal for the '<em><b>Messages</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PMESSAGE__MESSAGES = eINSTANCE.getPMessage_Messages();

		/**
		 * The meta object literal for the '<em><b>Options</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PMESSAGE__OPTIONS = eINSTANCE.getPMessage_Options();

		/**
		 * The meta object literal for the '<em><b>Fields</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PMESSAGE__FIELDS = eINSTANCE.getPMessage_Fields();

		/**
		 * The meta object literal for the '<em><b>Oneof</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PMESSAGE__ONEOF = eINSTANCE.getPMessage_Oneof();

		/**
		 * The meta object literal for the '<em><b>Reserved</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PMESSAGE__RESERVED = eINSTANCE.getPMessage_Reserved();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PMESSAGE__NAME = eINSTANCE.getPMessage_Name();

		/**
		 * The meta object literal for the '{@link protobuf.impl.PMessageFieldImpl <em>PMessage Field</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see protobuf.impl.PMessageFieldImpl
		 * @see protobuf.impl.ProtobufPackageImpl#getPMessageField()
		 * @generated
		 */
		EClass PMESSAGE_FIELD = eINSTANCE.getPMessageField();

		/**
		 * The meta object literal for the '<em><b>Options</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PMESSAGE_FIELD__OPTIONS = eINSTANCE.getPMessageField_Options();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PMESSAGE_FIELD__NAME = eINSTANCE.getPMessageField_Name();

		/**
		 * The meta object literal for the '<em><b>Repeated</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PMESSAGE_FIELD__REPEATED = eINSTANCE.getPMessageField_Repeated();

		/**
		 * The meta object literal for the '<em><b>Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PMESSAGE_FIELD__NUMBER = eINSTANCE.getPMessageField_Number();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PMESSAGE_FIELD__TYPE = eINSTANCE.getPMessageField_Type();

		/**
		 * The meta object literal for the '{@link protobuf.impl.POneOfImpl <em>POne Of</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see protobuf.impl.POneOfImpl
		 * @see protobuf.impl.ProtobufPackageImpl#getPOneOf()
		 * @generated
		 */
		EClass PONE_OF = eINSTANCE.getPOneOf();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PONE_OF__NAME = eINSTANCE.getPOneOf_Name();

		/**
		 * The meta object literal for the '<em><b>Options</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PONE_OF__OPTIONS = eINSTANCE.getPOneOf_Options();

		/**
		 * The meta object literal for the '<em><b>Fields</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PONE_OF__FIELDS = eINSTANCE.getPOneOf_Fields();

		/**
		 * The meta object literal for the '{@link protobuf.impl.PMapFieldImpl <em>PMap Field</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see protobuf.impl.PMapFieldImpl
		 * @see protobuf.impl.ProtobufPackageImpl#getPMapField()
		 * @generated
		 */
		EClass PMAP_FIELD = eINSTANCE.getPMapField();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PMAP_FIELD__NAME = eINSTANCE.getPMapField_Name();

		/**
		 * The meta object literal for the '<em><b>Options</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PMAP_FIELD__OPTIONS = eINSTANCE.getPMapField_Options();

		/**
		 * The meta object literal for the '<em><b>Key Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PMAP_FIELD__KEY_TYPE = eINSTANCE.getPMapField_KeyType();

		/**
		 * The meta object literal for the '<em><b>Value Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PMAP_FIELD__VALUE_TYPE = eINSTANCE.getPMapField_ValueType();

		/**
		 * The meta object literal for the '<em><b>Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PMAP_FIELD__NUMBER = eINSTANCE.getPMapField_Number();

		/**
		 * The meta object literal for the '{@link protobuf.impl.PReservedImpl <em>PReserved</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see protobuf.impl.PReservedImpl
		 * @see protobuf.impl.ProtobufPackageImpl#getPReserved()
		 * @generated
		 */
		EClass PRESERVED = eINSTANCE.getPReserved();

		/**
		 * The meta object literal for the '<em><b>Reserved Names</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PRESERVED__RESERVED_NAMES = eINSTANCE.getPReserved_ReservedNames();

		/**
		 * The meta object literal for the '<em><b>Reserved Ranges</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PRESERVED__RESERVED_RANGES = eINSTANCE.getPReserved_ReservedRanges();

		/**
		 * The meta object literal for the '{@link protobuf.impl.PRangeImpl <em>PRange</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see protobuf.impl.PRangeImpl
		 * @see protobuf.impl.ProtobufPackageImpl#getPRange()
		 * @generated
		 */
		EClass PRANGE = eINSTANCE.getPRange();

		/**
		 * The meta object literal for the '<em><b>Lower</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PRANGE__LOWER = eINSTANCE.getPRange_Lower();

		/**
		 * The meta object literal for the '<em><b>Upper</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PRANGE__UPPER = eINSTANCE.getPRange_Upper();

		/**
		 * The meta object literal for the '{@link protobuf.impl.PotoBufFileImpl <em>Poto Buf File</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see protobuf.impl.PotoBufFileImpl
		 * @see protobuf.impl.ProtobufPackageImpl#getPotoBufFile()
		 * @generated
		 */
		EClass POTO_BUF_FILE = eINSTANCE.getPotoBufFile();

		/**
		 * The meta object literal for the '<em><b>Enums</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference POTO_BUF_FILE__ENUMS = eINSTANCE.getPotoBufFile_Enums();

		/**
		 * The meta object literal for the '<em><b>Messages</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference POTO_BUF_FILE__MESSAGES = eINSTANCE.getPotoBufFile_Messages();

		/**
		 * The meta object literal for the '<em><b>Services</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference POTO_BUF_FILE__SERVICES = eINSTANCE.getPotoBufFile_Services();

		/**
		 * The meta object literal for the '<em><b>Options</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference POTO_BUF_FILE__OPTIONS = eINSTANCE.getPotoBufFile_Options();

		/**
		 * The meta object literal for the '<em><b>Imports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference POTO_BUF_FILE__IMPORTS = eINSTANCE.getPotoBufFile_Imports();

		/**
		 * The meta object literal for the '<em><b>Packages</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference POTO_BUF_FILE__PACKAGES = eINSTANCE.getPotoBufFile_Packages();

		/**
		 * The meta object literal for the '{@link protobuf.impl.PServiceImpl <em>PService</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see protobuf.impl.PServiceImpl
		 * @see protobuf.impl.ProtobufPackageImpl#getPService()
		 * @generated
		 */
		EClass PSERVICE = eINSTANCE.getPService();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PSERVICE__NAME = eINSTANCE.getPService_Name();

		/**
		 * The meta object literal for the '<em><b>Options</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PSERVICE__OPTIONS = eINSTANCE.getPService_Options();

		/**
		 * The meta object literal for the '<em><b>Rpc</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PSERVICE__RPC = eINSTANCE.getPService_Rpc();

		/**
		 * The meta object literal for the '{@link protobuf.impl.PRemoteProcedureCallImpl <em>PRemote Procedure Call</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see protobuf.impl.PRemoteProcedureCallImpl
		 * @see protobuf.impl.ProtobufPackageImpl#getPRemoteProcedureCall()
		 * @generated
		 */
		EClass PREMOTE_PROCEDURE_CALL = eINSTANCE.getPRemoteProcedureCall();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PREMOTE_PROCEDURE_CALL__NAME = eINSTANCE.getPRemoteProcedureCall_Name();

		/**
		 * The meta object literal for the '<em><b>Input Message Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PREMOTE_PROCEDURE_CALL__INPUT_MESSAGE_TYPE = eINSTANCE.getPRemoteProcedureCall_InputMessageType();

		/**
		 * The meta object literal for the '<em><b>Return Message Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PREMOTE_PROCEDURE_CALL__RETURN_MESSAGE_TYPE = eINSTANCE.getPRemoteProcedureCall_ReturnMessageType();

		/**
		 * The meta object literal for the '<em><b>Input Stream</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PREMOTE_PROCEDURE_CALL__INPUT_STREAM = eINSTANCE.getPRemoteProcedureCall_InputStream();

		/**
		 * The meta object literal for the '<em><b>Return Stream</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PREMOTE_PROCEDURE_CALL__RETURN_STREAM = eINSTANCE.getPRemoteProcedureCall_ReturnStream();

		/**
		 * The meta object literal for the '<em><b>Options</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PREMOTE_PROCEDURE_CALL__OPTIONS = eINSTANCE.getPRemoteProcedureCall_Options();

		/**
		 * The meta object literal for the '{@link protobuf.impl.PImportImpl <em>PImport</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see protobuf.impl.PImportImpl
		 * @see protobuf.impl.ProtobufPackageImpl#getPImport()
		 * @generated
		 */
		EClass PIMPORT = eINSTANCE.getPImport();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PIMPORT__NAME = eINSTANCE.getPImport_Name();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PIMPORT__TYPE = eINSTANCE.getPImport_Type();

		/**
		 * The meta object literal for the '{@link protobuf.impl.PPackageImpl <em>PPackage</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see protobuf.impl.PPackageImpl
		 * @see protobuf.impl.ProtobufPackageImpl#getPPackage()
		 * @generated
		 */
		EClass PPACKAGE = eINSTANCE.getPPackage();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PPACKAGE__NAME = eINSTANCE.getPPackage_Name();

		/**
		 * The meta object literal for the '{@link protobuf.PPrimitive <em>PPrimitive</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see protobuf.PPrimitive
		 * @see protobuf.impl.ProtobufPackageImpl#getPPrimitive()
		 * @generated
		 */
		EEnum PPRIMITIVE = eINSTANCE.getPPrimitive();

		/**
		 * The meta object literal for the '{@link protobuf.PImportType <em>PImport Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see protobuf.PImportType
		 * @see protobuf.impl.ProtobufPackageImpl#getPImportType()
		 * @generated
		 */
		EEnum PIMPORT_TYPE = eINSTANCE.getPImportType();

	}

} //ProtobufPackage
