# Modélisation
L'ensemble du langage Protocol Buffer a été modélisé pour ce projet.
La version du langage utilisé pour cette modélisation et la version 3 conformément à la spécification définit ici : https://developers.google.com/protocol-buffers/docs/reference/proto3-spec.
Cette version n'est pas la dernière mineure et elle n'a pas les mots-clés `optional` et `required` (plus de detail dans la partie transformation).
De plus les éléments suivants n'ont pas été modélisé (ils n'ont pas été corrigé/ajouter par ce qu'il ne sont pas nécessaires au projet et que je m'en suis aperçu trop tard) :
- La relation entre `message` et les champs de type `map` n'est pas défini (un `message` devrait contenir un champ de type `map`),
- Les champs des `oneof` sont actuellement définis comme identique aux champs des `message` mais ils ne peuvent cependant pas être `repeated`,
- La relation entre `enum` et les déclarations `reserved` (une `enum` devrait pouvoir contenir des déclarations `reserved`),

# Transformation
La règle de transformation initiale est `UML2Proto` :
en utilisant deux helpers `selectEnums` et `selectMessage`,
elle sélectionne toutes les énumérations UML et toutes les opérations de toutes les interfaces UML
puis les convertis en enumeration ProtoBuf et message ProtoBuf respectivement
en appelant les lazy rules `Enum2Enum` et `Operation2Message`.

La règle `Enum2Enum` est unique afin de pouvoir être utilisé sans duplication de l'énumération
lorsqu'une référence lui est faite dans le type d'un paramètre d'une opération.
`Enum2Enum` copie le nom de l'énumération et appelle la règle `EnumField2EnumField`
pour chaque champ copiant ainsi son nom et définissant la valeur unique associé en utilisant l'indice dans la liste.

La règle `Operation2Message` va convertir une opération UML en message ProtoBuf.
Le message est nommé avec le nom de l'interface qui contient l'opération suivit du nom cette opération :
cela permet d'éviter un conflit de nom s'il y a deux opérations de même nom situé dans deux interfaces différentes.
Comme tous les champs d'un message ProtoBuf doivent être identifié avec un nombre unique,
on initialise un compteur (`argument_id`) à 0 qui sera incrémenté à chaque champ puis remis à 0 à la fin pour le message suivant.
Pour les champs, ils se séparent en deux catégories :
- les champs avec une cardinalité `[0-1]` qui seront convertis vers un champ de type `oneof`,
- et les champs avec une autre cardinalité (`[1-1]`, `[0-*]`) qui seront convertis vers un champ de message standard.

La règle `Argument2OneOf` convertie un paramètre optionnel (cardinalité `[0-1]`) comme suit :
```
oneof parameter_name {
    type parameter_name = value
}
```
Un champ de type `oneof` permet de définir un choix exclusif parmi les champs qu'il contient,
cependant, il permet aussi de n'avoir aucune valeur (c'est-à-dire la valeur `null`).
Cela permet donc de définir une valeur comme étant optionnel.
La règle `Argument2OneOf` utilise la règle `Argument2Field` pour définir son champ unique dans `oneof`
(les champs de `message` et de `oneof` ont la même classe dans le méta-modèle).

La règle `Argument2Field` convertie un paramètre obligatoire (`[1-1]`) ou multiple (`[0-*]`).
Dans le cas d'un paramètre répété (`upper > 1`), le champ est défini `repeated`.
Le numéro unique du champ est défini avec le compteur `argument_id`
défini dans la règle `Operation2Message`, et sera incrémenté après le champ
pour garantir l'unicité du numéro au sein du message.
Le type est une référence vers
- un type primitif qui sera converti avec la règle `Primitive2Primitive`
qui fait le mapping des types UML vers ProtoBuf.
Lorsqu'un type est inconnu, le type par défaut `string` est attribué.
Cette règle est unique pour que chaque type primitif ne soit défini qu'une seule fois.
- un type énumération qui a été défini avec `Enum2Enum` et qui sera retrouvé
avec l'appel de cette même règle sur la même instance d'énumération UML
(sans que cette énumération soit de nouveau convertie puisque `Enum2Enum` est une règle unique)

# Génération
Le générateur de code prend le modèle en entrée et génère
l'ensemble des différentes sections du code ProtoBuf
(packages, imports, options, services, enumerations, messages)
conformément aux spécifications décrites dans ce document https://developers.google.com/protocol-buffers/docs/reference/proto3-spec.

Il est possible que le code généré contienne plusieurs lignes vide en raison d'un
certain nombre d'éléments vides : packages, imports, options, services ne sont pas
utilisés, mais il y a un retour à la ligne même lorsque la liste est vide.
