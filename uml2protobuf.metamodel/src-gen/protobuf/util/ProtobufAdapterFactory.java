/**
 */
package protobuf.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import protobuf.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see protobuf.ProtobufPackage
 * @generated
 */
public class ProtobufAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ProtobufPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProtobufAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = ProtobufPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProtobufSwitch<Adapter> modelSwitch = new ProtobufSwitch<Adapter>() {
		@Override
		public Adapter casePType(PType object) {
			return createPTypeAdapter();
		}

		@Override
		public Adapter casePTypePrimitive(PTypePrimitive object) {
			return createPTypePrimitiveAdapter();
		}

		@Override
		public Adapter casePTypeEnum(PTypeEnum object) {
			return createPTypeEnumAdapter();
		}

		@Override
		public Adapter casePEnumField(PEnumField object) {
			return createPEnumFieldAdapter();
		}

		@Override
		public Adapter casePOptionValue(POptionValue object) {
			return createPOptionValueAdapter();
		}

		@Override
		public Adapter casePConstant(PConstant object) {
			return createPConstantAdapter();
		}

		@Override
		public Adapter casePIdent(PIdent object) {
			return createPIdentAdapter();
		}

		@Override
		public Adapter casePLiteral(PLiteral object) {
			return createPLiteralAdapter();
		}

		@Override
		public Adapter casePStringLiteral(PStringLiteral object) {
			return createPStringLiteralAdapter();
		}

		@Override
		public Adapter casePIntLiteral(PIntLiteral object) {
			return createPIntLiteralAdapter();
		}

		@Override
		public Adapter casePFloatLiteral(PFloatLiteral object) {
			return createPFloatLiteralAdapter();
		}

		@Override
		public Adapter casePBoolLiteral(PBoolLiteral object) {
			return createPBoolLiteralAdapter();
		}

		@Override
		public Adapter casePMessage(PMessage object) {
			return createPMessageAdapter();
		}

		@Override
		public Adapter casePMessageField(PMessageField object) {
			return createPMessageFieldAdapter();
		}

		@Override
		public Adapter casePOneOf(POneOf object) {
			return createPOneOfAdapter();
		}

		@Override
		public Adapter casePMapField(PMapField object) {
			return createPMapFieldAdapter();
		}

		@Override
		public Adapter casePReserved(PReserved object) {
			return createPReservedAdapter();
		}

		@Override
		public Adapter casePRange(PRange object) {
			return createPRangeAdapter();
		}

		@Override
		public Adapter casePotoBufFile(PotoBufFile object) {
			return createPotoBufFileAdapter();
		}

		@Override
		public Adapter casePService(PService object) {
			return createPServiceAdapter();
		}

		@Override
		public Adapter casePRemoteProcedureCall(PRemoteProcedureCall object) {
			return createPRemoteProcedureCallAdapter();
		}

		@Override
		public Adapter casePImport(PImport object) {
			return createPImportAdapter();
		}

		@Override
		public Adapter casePPackage(PPackage object) {
			return createPPackageAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link protobuf.PType <em>PType</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see protobuf.PType
	 * @generated
	 */
	public Adapter createPTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link protobuf.PTypePrimitive <em>PType Primitive</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see protobuf.PTypePrimitive
	 * @generated
	 */
	public Adapter createPTypePrimitiveAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link protobuf.PTypeEnum <em>PType Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see protobuf.PTypeEnum
	 * @generated
	 */
	public Adapter createPTypeEnumAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link protobuf.PEnumField <em>PEnum Field</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see protobuf.PEnumField
	 * @generated
	 */
	public Adapter createPEnumFieldAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link protobuf.POptionValue <em>POption Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see protobuf.POptionValue
	 * @generated
	 */
	public Adapter createPOptionValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link protobuf.PConstant <em>PConstant</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see protobuf.PConstant
	 * @generated
	 */
	public Adapter createPConstantAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link protobuf.PIdent <em>PIdent</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see protobuf.PIdent
	 * @generated
	 */
	public Adapter createPIdentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link protobuf.PLiteral <em>PLiteral</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see protobuf.PLiteral
	 * @generated
	 */
	public Adapter createPLiteralAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link protobuf.PStringLiteral <em>PString Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see protobuf.PStringLiteral
	 * @generated
	 */
	public Adapter createPStringLiteralAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link protobuf.PIntLiteral <em>PInt Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see protobuf.PIntLiteral
	 * @generated
	 */
	public Adapter createPIntLiteralAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link protobuf.PFloatLiteral <em>PFloat Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see protobuf.PFloatLiteral
	 * @generated
	 */
	public Adapter createPFloatLiteralAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link protobuf.PBoolLiteral <em>PBool Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see protobuf.PBoolLiteral
	 * @generated
	 */
	public Adapter createPBoolLiteralAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link protobuf.PMessage <em>PMessage</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see protobuf.PMessage
	 * @generated
	 */
	public Adapter createPMessageAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link protobuf.PMessageField <em>PMessage Field</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see protobuf.PMessageField
	 * @generated
	 */
	public Adapter createPMessageFieldAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link protobuf.POneOf <em>POne Of</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see protobuf.POneOf
	 * @generated
	 */
	public Adapter createPOneOfAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link protobuf.PMapField <em>PMap Field</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see protobuf.PMapField
	 * @generated
	 */
	public Adapter createPMapFieldAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link protobuf.PReserved <em>PReserved</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see protobuf.PReserved
	 * @generated
	 */
	public Adapter createPReservedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link protobuf.PRange <em>PRange</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see protobuf.PRange
	 * @generated
	 */
	public Adapter createPRangeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link protobuf.PotoBufFile <em>Poto Buf File</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see protobuf.PotoBufFile
	 * @generated
	 */
	public Adapter createPotoBufFileAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link protobuf.PService <em>PService</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see protobuf.PService
	 * @generated
	 */
	public Adapter createPServiceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link protobuf.PRemoteProcedureCall <em>PRemote Procedure Call</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see protobuf.PRemoteProcedureCall
	 * @generated
	 */
	public Adapter createPRemoteProcedureCallAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link protobuf.PImport <em>PImport</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see protobuf.PImport
	 * @generated
	 */
	public Adapter createPImportAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link protobuf.PPackage <em>PPackage</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see protobuf.PPackage
	 * @generated
	 */
	public Adapter createPPackageAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //ProtobufAdapterFactory
