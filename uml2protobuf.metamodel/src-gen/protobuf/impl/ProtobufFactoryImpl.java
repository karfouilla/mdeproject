/**
 */
package protobuf.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import protobuf.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ProtobufFactoryImpl extends EFactoryImpl implements ProtobufFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ProtobufFactory init() {
		try {
			ProtobufFactory theProtobufFactory = (ProtobufFactory) EPackage.Registry.INSTANCE
					.getEFactory(ProtobufPackage.eNS_URI);
			if (theProtobufFactory != null) {
				return theProtobufFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ProtobufFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProtobufFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case ProtobufPackage.PTYPE_PRIMITIVE:
			return createPTypePrimitive();
		case ProtobufPackage.PTYPE_ENUM:
			return createPTypeEnum();
		case ProtobufPackage.PENUM_FIELD:
			return createPEnumField();
		case ProtobufPackage.POPTION_VALUE:
			return createPOptionValue();
		case ProtobufPackage.PIDENT:
			return createPIdent();
		case ProtobufPackage.PSTRING_LITERAL:
			return createPStringLiteral();
		case ProtobufPackage.PINT_LITERAL:
			return createPIntLiteral();
		case ProtobufPackage.PFLOAT_LITERAL:
			return createPFloatLiteral();
		case ProtobufPackage.PBOOL_LITERAL:
			return createPBoolLiteral();
		case ProtobufPackage.PMESSAGE:
			return createPMessage();
		case ProtobufPackage.PMESSAGE_FIELD:
			return createPMessageField();
		case ProtobufPackage.PONE_OF:
			return createPOneOf();
		case ProtobufPackage.PMAP_FIELD:
			return createPMapField();
		case ProtobufPackage.PRESERVED:
			return createPReserved();
		case ProtobufPackage.PRANGE:
			return createPRange();
		case ProtobufPackage.POTO_BUF_FILE:
			return createPotoBufFile();
		case ProtobufPackage.PSERVICE:
			return createPService();
		case ProtobufPackage.PREMOTE_PROCEDURE_CALL:
			return createPRemoteProcedureCall();
		case ProtobufPackage.PIMPORT:
			return createPImport();
		case ProtobufPackage.PPACKAGE:
			return createPPackage();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
		case ProtobufPackage.PPRIMITIVE:
			return createPPrimitiveFromString(eDataType, initialValue);
		case ProtobufPackage.PIMPORT_TYPE:
			return createPImportTypeFromString(eDataType, initialValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
		case ProtobufPackage.PPRIMITIVE:
			return convertPPrimitiveToString(eDataType, instanceValue);
		case ProtobufPackage.PIMPORT_TYPE:
			return convertPImportTypeToString(eDataType, instanceValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PTypePrimitive createPTypePrimitive() {
		PTypePrimitiveImpl pTypePrimitive = new PTypePrimitiveImpl();
		return pTypePrimitive;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PTypeEnum createPTypeEnum() {
		PTypeEnumImpl pTypeEnum = new PTypeEnumImpl();
		return pTypeEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PEnumField createPEnumField() {
		PEnumFieldImpl pEnumField = new PEnumFieldImpl();
		return pEnumField;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public POptionValue createPOptionValue() {
		POptionValueImpl pOptionValue = new POptionValueImpl();
		return pOptionValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PIdent createPIdent() {
		PIdentImpl pIdent = new PIdentImpl();
		return pIdent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PStringLiteral createPStringLiteral() {
		PStringLiteralImpl pStringLiteral = new PStringLiteralImpl();
		return pStringLiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PIntLiteral createPIntLiteral() {
		PIntLiteralImpl pIntLiteral = new PIntLiteralImpl();
		return pIntLiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PFloatLiteral createPFloatLiteral() {
		PFloatLiteralImpl pFloatLiteral = new PFloatLiteralImpl();
		return pFloatLiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PBoolLiteral createPBoolLiteral() {
		PBoolLiteralImpl pBoolLiteral = new PBoolLiteralImpl();
		return pBoolLiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PMessage createPMessage() {
		PMessageImpl pMessage = new PMessageImpl();
		return pMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PMessageField createPMessageField() {
		PMessageFieldImpl pMessageField = new PMessageFieldImpl();
		return pMessageField;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public POneOf createPOneOf() {
		POneOfImpl pOneOf = new POneOfImpl();
		return pOneOf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PMapField createPMapField() {
		PMapFieldImpl pMapField = new PMapFieldImpl();
		return pMapField;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PReserved createPReserved() {
		PReservedImpl pReserved = new PReservedImpl();
		return pReserved;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PRange createPRange() {
		PRangeImpl pRange = new PRangeImpl();
		return pRange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PotoBufFile createPotoBufFile() {
		PotoBufFileImpl potoBufFile = new PotoBufFileImpl();
		return potoBufFile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PService createPService() {
		PServiceImpl pService = new PServiceImpl();
		return pService;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PRemoteProcedureCall createPRemoteProcedureCall() {
		PRemoteProcedureCallImpl pRemoteProcedureCall = new PRemoteProcedureCallImpl();
		return pRemoteProcedureCall;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PImport createPImport() {
		PImportImpl pImport = new PImportImpl();
		return pImport;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PPackage createPPackage() {
		PPackageImpl pPackage = new PPackageImpl();
		return pPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PPrimitive createPPrimitiveFromString(EDataType eDataType, String initialValue) {
		PPrimitive result = PPrimitive.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPPrimitiveToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PImportType createPImportTypeFromString(EDataType eDataType, String initialValue) {
		PImportType result = PImportType.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPImportTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProtobufPackage getProtobufPackage() {
		return (ProtobufPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ProtobufPackage getPackage() {
		return ProtobufPackage.eINSTANCE;
	}

} //ProtobufFactoryImpl
