package uml2protobuf.codegenerator

import protobuf.PotoBufFile
import protobuf.PImport
import protobuf.PPackage
import protobuf.POptionValue
import protobuf.PIdent
import protobuf.PStringLiteral
import protobuf.PIntLiteral
import protobuf.PFloatLiteral
import protobuf.PBoolLiteral
import protobuf.PService
import protobuf.PRemoteProcedureCall
import protobuf.PTypePrimitive
import protobuf.PTypeEnum
import protobuf.PMessage
import protobuf.PEnumField
import protobuf.PReserved
import protobuf.PRange
import protobuf.PMessageField
import protobuf.POneOf

class ProtobufCodeGenerator {
	// identifier and literals
	def String generateQuoteString(String str) {
		return '"'+str.replace("\\", "\\\\").replace('"', '\\"')+'"'
	}
	def dispatch String generateConstCode(PIdent ident) {
		return ident.name
	}
	def dispatch String generateConstCode(PStringLiteral stringLit) {
		return generateQuoteString(stringLit.value)
	}
	def dispatch String generateConstCode(PIntLiteral intLit) {
		return intLit.value.toString
	}
	def dispatch String generateConstCode(PFloatLiteral floatLit) {
		return floatLit.value.toString
	}
	def dispatch String generateConstCode(PBoolLiteral boolLit) {
		return (boolLit.value ? "true" : "false")
	}

	// headers/other (package/import/option)
	def String generateCode(PPackage packageLine) '''
		package «packageLine.name»;
	'''
	def String generateCode(PImport importLine) '''
		import «importLine.type.literal» «importLine.name»;
	'''
	def String generateLong(POptionValue option) '''
		option «option.name» = «option.value.generateConstCode»;
	'''
	def String generateShort(POptionValue option) '''
		«option.name» = «option.value.generateConstCode»
	'''
	def String generateCode(PReserved reserved) '''
		«FOR reservedName : reserved.reservedNames BEFORE "reserved " SEPARATOR "," AFTER ";"»
			«generateQuoteString(reservedName)»
		«ENDFOR»
		«FOR reservedRange : reserved.reservedRanges BEFORE "reserved " SEPARATOR "," AFTER ";"»
			«reservedRange.generateCode»
		«ENDFOR»
	'''
	def String generateCode(PRange range) '''
		«range.lower»«IF range.lower != range.upper»to «IF range.upper > 0»«range.upper»«ELSE»max«ENDIF»«ENDIF»
	'''

	// services
	def String generateCode(PRemoteProcedureCall rpc) '''
		rpc «rpc.name» («IF rpc.inputStream»stream «ENDIF»«rpc.inputMessageType.generateTypeName»)
		returns («IF rpc.returnStream»stream «ENDIF»«rpc.returnMessageType.generateTypeName»)«
		FOR POptionValue option : rpc.options BEFORE " {" AFTER "}"»«
			option.generateLong
		»«ENDFOR»;
	'''
	def String generateCode(PService service) '''
		service «service.name» {
			«FOR POptionValue option : service.options»
				«option.generateLong»
			«ENDFOR»

			«FOR PRemoteProcedureCall rpc : service.rpc»
				«rpc.generateCode»
			«ENDFOR»
		}
	'''

	// types (enums/messages)
	def dispatch String generateTypeName(PTypePrimitive primitive) {
		return primitive.type.literal
	}
	def dispatch String generateTypeName(PTypeEnum enumeration) {
		return enumeration.name
	}
	def dispatch String generateTypeName(PMessage message) {
		return message.name
	}
	// enum
	def String generateCode(PEnumField field) '''
		«field.name» = «field.value»«
		FOR POptionValue option : field.options BEFORE " [" SEPARATOR "," AFTER "]"»«
			option.generateShort
		»«ENDFOR»;
	'''
	def String generateCode(PTypeEnum enumeration) '''
		enum «enumeration.name» {
			«FOR PEnumField field : enumeration.fields»
				«field.generateCode»
			«ENDFOR»
			«FOR POptionValue option : enumeration.options BEFORE "\n"»
				«option.generateLong»
			«ENDFOR»
		}
	'''
	// message
	def String generateCode(PMessageField field) '''
		«IF field.repeated»repeated «ENDIF»«field.type.generateTypeName» «field.name» = «field.number»«
		FOR POptionValue option : field.options BEFORE " [" SEPARATOR "," AFTER "]"»«
			option.generateShort
		»«ENDFOR»;
	'''
	def String generateCode(POneOf oneof) '''
		oneof «oneof.name» {
			«FOR PMessageField field : oneof.fields»
				«field.generateCode»
			«ENDFOR»
			«FOR POptionValue option : oneof.options»
				«option.generateLong»
			«ENDFOR»
		}
	'''
	def String generateCode(PMessage message) '''
		message «message.name» {
			«FOR PReserved reserved : message.reserved»
				«reserved.generateCode»
			«ENDFOR»
			«FOR PTypeEnum enumeration : message.enums»
				«enumeration.generateCode»
			«ENDFOR»
			«FOR PMessage subMessage : message.messages»
				«subMessage.generateCode»
			«ENDFOR»
			«FOR POneOf oneof : message.oneof»
				«oneof.generateCode»
			«ENDFOR»
			«FOR PMessageField field : message.fields»
				«field.generateCode»
			«ENDFOR»
			«FOR POptionValue option : message.options»
				«option.generateLong»
			«ENDFOR»
		}
	'''

	// main
	def String generateCode(PotoBufFile protoBufModel) '''
		syntax = "proto3";
		«FOR PPackage packages : protoBufModel.packages»
			«packages.generateCode»
		«ENDFOR»
		«FOR PImport imports : protoBufModel.imports»
			«imports.generateCode»
		«ENDFOR»
		«FOR POptionValue option : protoBufModel.options»
			«option.generateLong»
		«ENDFOR»
		«FOR PService service : protoBufModel.services»
			«service.generateCode»
		«ENDFOR»

		«FOR PTypeEnum enumeration : protoBufModel.enums»
			«enumeration.generateCode»
		«ENDFOR»
		«FOR PMessage message : protoBufModel.messages»
			«message.generateCode»
		«ENDFOR»
	'''
}