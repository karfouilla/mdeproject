/**
 */
package protobuf;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see protobuf.ProtobufPackage
 * @generated
 */
public interface ProtobufFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ProtobufFactory eINSTANCE = protobuf.impl.ProtobufFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>PType Primitive</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PType Primitive</em>'.
	 * @generated
	 */
	PTypePrimitive createPTypePrimitive();

	/**
	 * Returns a new object of class '<em>PType Enum</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PType Enum</em>'.
	 * @generated
	 */
	PTypeEnum createPTypeEnum();

	/**
	 * Returns a new object of class '<em>PEnum Field</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PEnum Field</em>'.
	 * @generated
	 */
	PEnumField createPEnumField();

	/**
	 * Returns a new object of class '<em>POption Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>POption Value</em>'.
	 * @generated
	 */
	POptionValue createPOptionValue();

	/**
	 * Returns a new object of class '<em>PIdent</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PIdent</em>'.
	 * @generated
	 */
	PIdent createPIdent();

	/**
	 * Returns a new object of class '<em>PString Literal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PString Literal</em>'.
	 * @generated
	 */
	PStringLiteral createPStringLiteral();

	/**
	 * Returns a new object of class '<em>PInt Literal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PInt Literal</em>'.
	 * @generated
	 */
	PIntLiteral createPIntLiteral();

	/**
	 * Returns a new object of class '<em>PFloat Literal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PFloat Literal</em>'.
	 * @generated
	 */
	PFloatLiteral createPFloatLiteral();

	/**
	 * Returns a new object of class '<em>PBool Literal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PBool Literal</em>'.
	 * @generated
	 */
	PBoolLiteral createPBoolLiteral();

	/**
	 * Returns a new object of class '<em>PMessage</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PMessage</em>'.
	 * @generated
	 */
	PMessage createPMessage();

	/**
	 * Returns a new object of class '<em>PMessage Field</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PMessage Field</em>'.
	 * @generated
	 */
	PMessageField createPMessageField();

	/**
	 * Returns a new object of class '<em>POne Of</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>POne Of</em>'.
	 * @generated
	 */
	POneOf createPOneOf();

	/**
	 * Returns a new object of class '<em>PMap Field</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PMap Field</em>'.
	 * @generated
	 */
	PMapField createPMapField();

	/**
	 * Returns a new object of class '<em>PReserved</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PReserved</em>'.
	 * @generated
	 */
	PReserved createPReserved();

	/**
	 * Returns a new object of class '<em>PRange</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PRange</em>'.
	 * @generated
	 */
	PRange createPRange();

	/**
	 * Returns a new object of class '<em>Poto Buf File</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Poto Buf File</em>'.
	 * @generated
	 */
	PotoBufFile createPotoBufFile();

	/**
	 * Returns a new object of class '<em>PService</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PService</em>'.
	 * @generated
	 */
	PService createPService();

	/**
	 * Returns a new object of class '<em>PRemote Procedure Call</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PRemote Procedure Call</em>'.
	 * @generated
	 */
	PRemoteProcedureCall createPRemoteProcedureCall();

	/**
	 * Returns a new object of class '<em>PImport</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PImport</em>'.
	 * @generated
	 */
	PImport createPImport();

	/**
	 * Returns a new object of class '<em>PPackage</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PPackage</em>'.
	 * @generated
	 */
	PPackage createPPackage();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ProtobufPackage getProtobufPackage();

} //ProtobufFactory
