package uml2protobuf.testing

import java.nio.file.Files
import java.nio.file.Paths
import java.util.List
import java.util.stream.Collectors
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import uml2protobuf.testing.util.ATLHelper

/**
 * Run the IDLCodeGenerator on each IDL model found in "models/input/uml/*.uml".
 * 
 * Results are produced in "models/ouptput/protobuf/".
 */
class TransformationTestSuite {
	private def void transformFrom(String modelPath) {
		val String inputMetamodelPath = "http://www.eclipse.org/uml2/2.1.0/UML"
		val String inputMetamodelATLName = "UML"
		val String outputMetamodelPath = "../uml2protobuf.metamodel/model/protobuf.ecore"
		val String outputMetamodelATLName = "Protobuf"
		val String inputModelPath = modelPath
		val String transformationAsmPath = "../uml2protobuf.transformation/src/uml2protobuf.asm"
		
		val modelName = Paths::get(modelPath).fileName.toString
		val String outputModelPath = "models/output/protobuf/" + modelName.substring(0, modelName.length - 3) + "xmi"

		ATLHelper::transform(inputMetamodelPath, inputMetamodelATLName, outputMetamodelPath, outputMetamodelATLName,
			inputModelPath, transformationAsmPath, outputModelPath)
	}

	@ParameterizedTest
	@MethodSource("modelProvider")
	def void transform(String argument) {
		transformFrom(argument);
	}

	def static List<String> modelProvider() {
		return Files::list(Paths::get("models/input/uml/")).map([p|p.toString]).filter([s|s.endsWith(".uml")]).collect(
			Collectors.toList())
	}
}
